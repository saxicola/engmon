/**
Filename: simple.c
$Author: mikee $

Yacht monitor software for 68HC11 based board
Monitors mainly engine functions, and displays them in a menu controlled
LCD display.
Functions monitored:
RPM
Exhaust temp, could be related to RPM to give better performance.
Water temp
Oil pressure
Fuel flow (possible with low flow sensor.  Own design?)
Warning buzzer sounds if any parameter exceeded with display of warning on LCD.
If no RPM then no warnings also.

Records engine running time based on flywheel rotation counting NOT just power
on time which overestimates run time.  This should have greater accuracy even allowing
for some rounding errors.  Although hardly needed it is nice to be accurate!

A real time timer, count up or down, for racing starts can be accessed from the right key.

Where awkward decimal numbers occur, such as in thermistor conversion, these are converted
to a close approximation as a fraction.  Mostly this is accurate enough for our purposes.

$Id: EngMon.c,v 1.33 2004/05/13 20:59:44 mikee Exp $
$Revision: 1.33 $
*/
//-------------------------------------------------------------------------
#define  M6811_FAMILY_F1
#define USE_INTERRUPT_TABLE
#include <arch/exit.h>
#include <sys/sio.h>
#include <sys/ports.h>
#include <stdio.h>
//#include <math.h>
#include "EngMon.h"
#include "interrupts.h"
#include <eeprom.h>
#include <inttypes.h>

/* Include interrupts.h only if the vectors need changing.  We do not need to
write these EVERY upload as they are in EEPROM and therfore persitent */


#define PPI_PORTA     0x00	/* PPI Port A register */
#define PPI_PORTB     0x01	/* PPI Port A register */
#define PPI_PORTC     0x02	/* PPI Port A register */
#define PPI_CONTROL   0x03	/* PPI Port A register */
#define LCD_WAIT	50		/* The wait time in the LCD write routinr*/
//#define USE_INTERRUPT_TABLE // or #undef, if not

#define TICK	4E-6 // For TIC and TOC timings assumes PR=01
/* Key pad values */
#define KEY_UP  0x7
#define KEY_DOWN  0xE
#define KEY_RIGHT  0xB
#define KEY_LEFT  0xD

// For hex function
#define HEX_CVT_MASK 0x0fff

#define LOW_RPM 100 // Below this don't display warnings etc.

#define MAX_SCREENS 4 // Number of normal run screens

// Define alarn values as 2^N
#define ALARM_OIL 1
#define ALARM_WATER 2
#define ALARM_EXHAUST 4
#define ALARM_HEAD 8
#define ALARM_CHARGE 16
#define	ALARM_OTHER 32


extern volatile unsigned char _BASE[]; //Define an array starting at _RAM (0x00)
extern volatile unsigned char _PPI[]; // Location of PPI chip


/* Global variables stored in internal RAM */

unsigned int __attribute__ ((section (".data"))) rpm = 0, rpmIntCount = 0;
unsigned long __attribute__ ((section (".data")))rpmSum = 0;
char __attribute__ ((section (".data"))) time[] = "000:00:00", X = 10; // X for serial send timer
char  __attribute__ ((section (".data"))) c; // For serial receive
unsigned int __attribute__ ((section (".data"))) rpmCount1=0, rpmCount2=0; // First and second TIC1 values
unsigned char  __attribute__ ((section (".data"))) key = 0x00, lastKey;  // Which key (or combination) last pressed
unsigned int __attribute__ ((section (".data"))) tofCount = 10; // Timer overflow counter, 16 bits so overflow at 65536 * TCNT overflow time
unsigned int __attribute__ ((section (".data"))) tofRPMCount;
unsigned char __attribute__ ((section (".data")))ticks=0, seconds=0, minutes=0, dMinutes = 5, dSeconds = 0, hours = 0; // Timer storage stuff
char __attribute__ ((section (".data")))runmode = 1; // What mode are we in? 1 = normal engine running display, 2 = race timer display

unsigned char __attribute__ ((section (".data")))alarm = 0; // Alarms bool
// Runtime parameter storage
unsigned char __attribute__ ((section (".data")))oilPressure, exTemp, engTemp, fuelFlow, ambientTemp, seaTemp;
char __attribute__ ((section (".data")))null = 0;
char __attribute__ ((section (".data")))menuItem = 0,statusLine; ; // For the scrolling menu system// The currently displayed status line
char __attribute__ ((section (".data")))setValue; // For the temporary storage of the set value, later stored in EEPROM.  See next.
/* These global variables are persistent (over re-boots) and are stored in .eeprom section
NB: These cannot be written using simple assignment, they have to written to EEPROM
using an EEPROM writing routine. */
unsigned char __attribute__ ((section (".eeprom")))
exTempHi = 96,
engineTempHi = 65,
oilPressLo = 5,
defaultRunScreen = 0, // Can be set up in setup thang.  User can change in use, perhaps.
keyBeep = 1, // De we beep on key press?  Default = yes we certainly do!
backLight = 0, // Is the backlight on or off.  Default = off
rpmSensor = 1, // Is the rpm sensor present?
T[3];// Exhaust temperature values for cal curve.  See next.
unsigned int __attribute__ ((section (".eeprom")))RPM[3];//Speeds at which Exhaust temperature calibration refer
char __attribute__ ((section (".eeprom")))storage[8],
version; // This is the placeholder for the EPROM version number.
// Code checks this on startup and copies new data to EEPROM if not correct.
// Most EEPROM vars are at beginning so we only need to reload these
// if we add anything to change their position.  Vectors only need reloading if any of the interrupt
// methods change (load pos will change), so version only needs updating if these conditions arise
#define VERSION 8 // See above definition.

unsigned short __attribute__ ((section (".eeprom")))minutesRunCount = 0; // Engine runtime in minutes.  Updated each approx minute

extern void udelay (unsigned long usec);



// Run time screen definitions and messages.  A screen consists of two displayed lines
// Values are substituted at run time in the correct place (hopefully)
// Lines may be mix and match.  Warnings will overlay.
#define statusLines 5

char __attribute__ ((section (".data")))
runStatus[][17] = {
    {"0000 RPM   22PSI"},
    {"EX=000C  HD=080C"},
    {"1.2 AMPS CHARGE "},
    {"BATT 13.6 VOLTS "},
    {"FUEL      L/Hr  "},
    {"WINE      C     "}, // Retain extra blank line here
};

/** These are global constants and stored in .text section */
char  __attribute__ ((section (".text")))
revision[] = "$Revision: 1.33 $"; // The software CVS revision number
char  __attribute__ ((section (".text")))
warning[][17] = {
    {"LOW OIL PRESSURE"},
    {" NO INLET WATER "},
    {"  HIGH EX TEMP  "},
    {"HIGH ENGINE TEMP"},
    {"  NOT CHARGING  "},
    {"0123456789ABCDEF"},
    {"0123456789ABCDEF"},
    {"0123456789ABCDEF"}
};

char __attribute__ ((section (".text")))
setUpMessage[][17] = {

    {"SET EX ALARM"},
    {"SET WATER ALARM"},
    {"SET OIL ALARM"},
    {"SET KEY BEEP"},
    {"SET BACKLIGHT"},
    {"SET ENG HOURS"},
    {"RPM SENSOR?"}
};


/* END persitent global variables and constants */
//__attribute__ ((section (".text")))
//-------------------------------------------------------------------------
/**
This runs JUST after stack pointer initialised so we can put our pre-64
cycles code here.
When we get here we have 55 cycles remaining having used 9 in _start()
In order to get within limit this needs to be done in assembly code using;
asm volatile("assy	com ###") inlined assembly.
*/
void __premain()
{
    // Set TMSK2 to give us a timer tick of 4 us.  Enable TOI and RTII interrupt
    _io_ports[M6811_TMSK2] = 0xC2;

 	// X = reg base
    asm volatile ("ldx	#0x1000"); // 4 - 51
	// set BPROT to 0x00 to enable CONFIG and EEPROM PROGRAMMING
    asm volatile ("ldaa #0x00"); // 7 - 44
    asm volatile ("staa	0x35,X"); // 7 - 44

	// Dis/En able COP by setting CONFIG.  See page 30 of manual
	// 0xFF disables 0xFB enables
	asm volatile ("ldaa #0xFB"); // 7 - 44
    asm volatile ("staa	0x3F,X"); // 7 - 44

    // Switch to 9600 baud.
    //_io_ports[M6811_BAUD] = 0x30;
    //asm volatile ("ldaa	#0x30"); // 3 - 41
    //asm volatile ("staa	0x2B,X"); // 4 - 37


}

//-------------------------------------------------------------------------
/**
This gets called every 16.384ms.
Updates the clock.
Updates the engine running minutes counter
Calculate the RPM every second and update display
*/
void rti_handler(void)
{
    //_io_ports[M6811_TFLG1] = M6811_OC1F; /* Clear Timer Interrupt Flag Register 1 */
    _io_ports[M6811_TFLG2] |= 0x40; /* Clear Timer Interrupt Flag  */
    ticks += 1;
    if(ticks  == 61)// Do the seconds
    {
        //Beep(4,10);
        seconds ++;
        //dSeconds --;
        ticks = 0;

        if(rpmIntCount > 0)
		{
		rpm = (int)(rpmSum / rpmIntCount);
		rpmSum=0;
		rpmIntCount = 0;
		setRPM();
		}
        else {rpm /=2; setRPM();}

		if(!rpmSensor){rpm = 0;setRPM();}

        if(alarm != 0 )Beep(20,100);

    }


    // Send serial data every X seconds
    //if(seconds % 10 == 0)SendSerial();


    if(seconds == 60)// Do the minutes
    {
        minutes ++;
        if(rpm > 100 )eeprom_write_short (&minutesRunCount, minutesRunCount+1);
        seconds = 0;
        //SendSerial();
    }
    if(minutes == 60) // do the hours
    {
        hours += 1;
        minutes = 0;
    }

    char mins[]="  ", secs[] = "  ", hh[] = "   ";
    dec_convert(secs, seconds);
    dec_convert(mins, minutes);
    dec_convert(hh, hours);


    time[0] = hh[0];
    time[1] = hh[1];
    time[2] = hh[2];
    time[3] = ':';
    time[4] = mins[0];
    time[5] = mins[1];
    time[6] = ':';
    time[7] = secs[0];
    time[8] = secs[1];

}
//-------------------------------------------------------------------------
/**
Do this if the COP fails, like in a loop somewhere.  Just restart the program.
*/
void cop_fail_interrupt(void)
{
    _start();
}
//-------------------------------------------------------------------------
/**
TIC1 ISR.  Does RPM stuff.  Triggered on rising edge on TIC1.
If we are here then the engine is running so we also increase the engine
minutes counter.
Local timer records seconds run, when = 60 increment run time and zero local timer.
Probably no need to zero local timer after startup.  Will need local us timer.
*/
void rpm_interrupt(void)
{
	if(!rpmSensor)return;
    if(keyBeep)Beep(1,5);

    rpmCount1 = rpmCount2;
    rpmCount2 = _io_ports[M6811_TIC1_L] + (256 * _io_ports[M6811_TIC1_H]);

    unsigned int pCount = (rpmCount2 - rpmCount1);
    rpmSum += (long)(15000000 / (pCount + (tofCount*65536)));
    rpmIntCount++;
	tofCount = 0;
    _io_ports[M6811_TFLG2] = 0x80; // Clear TOF
    _io_ports[M6811_TFLG1] = M6811_IC1F; // Clear the flag to reset
}

//-------------------------------------------------------------------------
/**
Timer overflow interrupt.  Triggered when TOF set in TFLG2.
Increments tofCount, and resets flag by writing to register.
Decrements rpm slowly
*/
void timer_overflow_interrupt()
{
	#define MAX_OVERFLOWS 10
    // Increment counter
    tofCount++;
    if(tofCount == MAX_OVERFLOWS)// Engine stopped so decrease rpm slowly
    {
        if(rpm > 0)rpm /= 2; // Reduce it slowly, ish
        void setRPM();
        tofCount = 0;
    }
    _io_ports[M6811_TFLG2] = 0x80; // Clear TOF
}

//-------------------------------------------------------------------------
/**

Not used
*
void toc1_interrupt(void)
{


}
*/
//-------------------------------------------------------------------------
/**
*/
static void flush ()
{
    while (!(_io_ports[M6811_SCSR] & M6811_TDRE))
        continue;
}
//-------------------------------------------------------------------------
/**
Write the RPM to the display memory buffer
*/
void setRPM(void)
{
    char buf[] = "0000";
    dec_convert(buf, rpm);
    uint8_t n = 0;
    do{ runStatus[0][n] = buf[n]; } while(++n < 4);
}
//-------------------------------------------------------------------------
/** return analog value for port passed.  Since we scan only a single port here
with MULT=0 then the four addresses contain four conversion from the SAME port
and so we can average these for greater accuracy.
Analog ports are arranged as follows:
3 - Engine Cylinder head.  Aka water temperature. 	- 5K resistor
1 - Engine Exhaust temperature						- 810R resistor
2 - Engine oil pressure
0 - Sea temperature
6 - Battery voltage
7 - Charging current
4 - Fuel rate
5 -
@param char A/D port number.
*/
char read_analog(char channel)
{
    /* enable analog charge pump */
    _io_ports[M6811_OPTION] |= 0x80;
    delay_Nus (100); // Wait.  See Fox p121
    //char mode = 0x80;
    channel |= 0x80; // Combine them
    /* set A/D system for single scan on AD port*/
    _io_ports[M6811_ADCTL] = channel; // This clears CCF (bit 7).
    // Wait for result.  Until CCF set again
    while(_io_ports[M6811_ADCTL] != channel);
    // Turn off A/D
    _io_ports[M6811_OPTION] |= 0x00;
    return (char)((_io_ports[M6811_ADR1] + _io_ports[M6811_ADR2] +
	_io_ports[M6811_ADR3] + _io_ports[M6811_ADR4]) / 4 );
}

//-------------------------------------------------------------------------
/**
Do the user input via keyboard instruction
UP/DOWN scroll through different data display screens
Combinations of keys are usually allowed in normal mode
LEFT + RIGHT passes to SetAlarm()
*/
void doKeyboard(char key)
{
    if(key == 0x0F)return;
    cop_optional_reset();  //reset the COP each time

    if(keyBeep)Beep(1,40);
    switch(key){
    case KEY_LEFT: // Return to normal run mode
        runmode=1;
        break;

    case KEY_UP: // KEY_UP
        if(runmode == 1) // scroll up
        {
            statusLine ++;
            if(statusLine >= statusLines)statusLine = 0;
        }
        if(runmode == 2) // reset the time
        {
            seconds = 0; minutes = 0; hours = 0; ticks = 0;
        }
        if(runmode == 3)runmode = 3; // Do nothing

        break;

    case  KEY_LEFT & KEY_UP: //Turn on backlight perhaps?
        lcdWrite("LEFT & UP", 2);

        break;
    case KEY_DOWN: // KEY_DOWN
		statusLine = 0;
        break;

    case KEY_RIGHT: // KEY_RIGHT
        runmode++;
        if(runmode > 3)runmode = 1; // Recycle
        break;

    case KEY_UP & KEY_DOWN:
        TCurveCal();
        break;

    case KEY_RIGHT & KEY_LEFT: //Enter SetAlarms menu
        SetAlarms();
        break;


    }

}
//-------------------------------------------------------------------------
/**
Deal with setting up the alarms etc., from the keyboard.
KEY_LEFT sets the value and returns
*/
char SetAlarms()
{


    RunDisplay();
    menuItem =0;
    // Temporary values read from old stored values
    #define ITEMS 7 // The number of items in the settings list
    unsigned char  temp[ITEMS];

    temp[0] = exTempHi;    // default = 95,
    temp[1] = engineTempHi; // = 95,
    temp[2] = oilPressLo;  // = 5;
    temp[3] = keyBeep;
    temp[4] = backLight;
    temp[5] = (minutesRunCount)/60; // = Engine run time in hours
	temp[6] = rpmSensor; // Is the sensor there?

    char key; // must be outside loop
    do{
        char buf[] = "      ";
        key = readKeyBoard();
        if(key == KEY_UP)temp[menuItem]++;
        if(key == KEY_DOWN)temp[menuItem]--;
        if(key == KEY_RIGHT)menuItem++; // Only increment this in loop
        if(menuItem >= ITEMS)menuItem = 0;
        if(temp[3] == 2)  temp[3] = 1; // In the case of these we can only have 1 or 0
        if(temp[3] == 255)temp[3] = 0;
        if(temp[4] == 2)  temp[4] = 1;
        if(temp[4] == 255)temp[4] = 0;
		if(temp[6] == 2)  temp[6] = 1;
        if(temp[6] == 255)temp[6] = 0;
        // Display the Item and the previously set value for the item
        lcdWrite(setUpMessage[menuItem], 1);
        //serial_print("\r");serial_print(setUpMessage[menuItem]);serial_print(" : ");
        dec_convert(buf, temp[menuItem]);
        lcdWrite(buf, 2);
        //serial_print(buf);serial_print("           ");
        // If the LEFT key is pressed, write ALL the values into EEPROM and exit
        if(key == KEY_LEFT)
        {
            cop_optional_reset();  //reset the COP each time
            asm volatile ("sei"); // Disable interrupts
            // Set exhaust warning temperature
            eeprom_write_byte (&exTempHi, temp[0]);
            // Set engine temperature warning
            eeprom_write_byte (&engineTempHi, temp[1]);
            // Set oil pressure warning
            eeprom_write_byte (&oilPressLo, temp[2]);
            // Set key beep on or off
            eeprom_write_byte (&keyBeep, temp[3]);
            //Set back light on or off
            eeprom_write_byte (& backLight, temp[4]);
            // Write user set engine run time
            eeprom_write_short (&minutesRunCount, (temp[5])*60);
			// Define the presence of the rpm sensor
			eeprom_write_byte (&rpmSensor, (temp[6]));
            asm volatile ("cli"); // Enable interrupts
            serial_print("ALL CHANGED SETTING SAVED");serial_print("\n\r");
            key = 0x0f;
            Beep(2,100);
            return 0;
        }
        if (key == 0x0f)continue;
        if(keyBeep)Beep(10,50);
    }while(key != KEY_LEFT);
    //serial_print("\n\r");
    //serial_print("ALL SETTING SAVED");serial_print("\n\r");
    //menuItem = 0;
    return 0;
}
//-------------------------------------------------------------------------
/**
A monitor loop call functions that monitor keyboard and sensor inputs
meanwhile monitors the serial line
*/
char monitor()
{
    cop_optional_reset();  //reset the COP each time
    monSerial(); // Monitor the serial port
    uint8_t key = readKeyBoard(); // returns 0 if no key pressed
    getOilPress();
    doKeyboard(key);
    GetEngineTemperature();
    getExTemp();
	GetSeaTemp();
    //if(ticks % 10 ==  0)
	cop_optional_reset();  //reset the COP each time
    RunDisplay();
    return 1;
}

//-------------------------------------------------------------------------
/**
Convert a char to its decimal value equivalent
@param char* buf containing the string to return
@param long The hex value to be converted
*/
static char* dec_convert(char* buf, value_t value)
{
    char num[20];
    int pos = 0;
    char count = 0;
    while(buf[++count] != 0); // Get the buffer size count.  Relies on the EO string marker

    if (value < 0)
    {
        //*buf++ = '-';
        buf[0] = '-';
        value = -value;
    }

    while (value != 0)
    {
        char c = value % 10;
        value = value / 10;
        num[pos++] = c + '0';
    }

    if (pos == 0)
        num[pos++] = '0';
    buf[count--] = 0; // set end of buffer to '\0'

    while (--pos >= 0)
    {
        //*buf++ = num[pos];

        buf[count - pos] = num[pos];

    }

    //*buf = 0;
    return buf;
}


//-------------------------------------------------------------------------
/**
Convert a char to its hex value equivalent
@param char* buf containing the string to return
@param value_t The hex value to be converted
*/
static char*
hex_convert(char* buf, value_t value)
{
    char num[32];
    int pos;

    *buf++ = '0';
    *buf++ = 'x';

    pos = 0;
    while (value != 0)
    {
        char c = value & 0x0F;
        num[pos++] = "0123456789ABCDEF"[(unsigned) c];
        value = (value >> 4) & HEX_CVT_MASK;
    }
    if (pos == 0)
        num[pos++] = '0';

    while (--pos >= 0)
        *buf++ = num[pos];

    *buf = 0;
    return buf;
}

//-------------------------------------------------------------------------
/**
Set up the PPI chip into 'mode'.  C-HI is always input  in theis application
as this is the keyboard input.  Unless some special code disables keyboard!
The mode descriptions and code correspond to those in the Harris 82C55A data
sheet where mode(here) is equivalent to 'CONTROL WORD'.  We always operate
in mode0 (Harris 82C55A data sheet), simple IO.
Some mode Descriptions:

		PORT DIRECTION		CONTROL WORD (char mode)
	A	B	C-HI	C-LO	BIN		HEX
	O	O	O	O	1000,0000	0X80
	O	O	O	I	1000,0001	0x81
	O	I	O
	O	I	I	O
	O	O	I	O	1000,1000	0x88
	O	O	I	O


	I	O	O	O	1001,0000	0x90
	I	O	O	O	1001,0001	0x91
*/
void setup_PPI(char mode)
{
    _PPI[PPI_CONTROL]  = mode;
}



//-------------------------------------------------------------------------
/**
Set up LCD to mode and switch LCD on for writing.
Modes are:
4 bit, 2 lines, 5x8 chars
*/
void LCD_init(void)
{
    short
    E_HI = 0x04,
    RS_HI=0x01,
    RW_HI=0x02;
     #define LCD_WAIT  50
    _io_ports[M6811_DDRG] = 0x7F;
    // Now set up the LCD for 4 bit operation
    _io_ports[M6811_PORTG] = 0x18   | (E_HI & ~RS_HI & ~RW_HI);
    delay_Nus(LCD_WAIT);
    _io_ports[M6811_PORTG] = 0x18   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nms(5);
	_io_ports[M6811_PORTG] = 0x18  | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x18   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nms(5);


    //
	_io_ports[M6811_PORTG] = 0x10   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x10   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	//Now set up the LCD for 2 lines and 5x8 = 0x40
	_io_ports[M6811_PORTG] = 0x40   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x40   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);

	// Turn on display and cusor off
	_io_ports[M6811_PORTG] = 0x00   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x00   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x60   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x60   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);

	// Clear and home the cursor
	_io_ports[M6811_PORTG] = 0x00   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x00   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x01   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x01   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);


	_io_ports[M6811_PORTG] = 0x00   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x00   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x30   | (E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);
	_io_ports[M6811_PORTG] = 0x30   & (~E_HI & ~RS_HI & ~RW_HI);
	delay_Nus(LCD_WAIT);

}

//-------------------------------------------------------------------------
/**
Write a string to the LCD display.  This is a two line 32 char Hitachi HD44780.
It is attached  on port G and control. This
function assumes that the message is a zero (\0) terminated string.
@param char* The message string
@param char Which line to write to
@return short LCD Status
*/
char
lcdWrite(char * message, char line)
{    short
    E_HI = 0x04,
    RS_HI=0x01,
    RW_HI=0x02;
    #define LCD_WAIT  50
    // Set the PPI so we can write to the LCD


    if(line == 2)
	{
		_io_ports[M6811_PORTG] = (((0x60 | E_HI) & ~ RS_HI) & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = (((0x60   & ~E_HI) & ~ RS_HI) & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = (((0x00   | E_HI) & ~ RS_HI) & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = (((0x00   & ~E_HI) & ~ RS_HI)  & ~RW_HI);
		delay_Nus(LCD_WAIT);
	}

	else
	{
		_io_ports[M6811_PORTG] = 0x00   | (E_HI & ~ RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = 0x00   & (~E_HI & ~ RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = 0x10   | (E_HI & ~ RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = 0x10   & (~E_HI & ~ RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
	}


	uint8_t l = 0;
    // Write the letters of the message to the LCD.  Max line length = 16
	while(message[l] != '\0')
	{
		char hiByte = message[l];
		hiByte >>= 4;
		hiByte <<=3;
		char loByte = message[l];
		loByte <<=3;
		loByte &= ~0x7;


		_io_ports[M6811_PORTG] = hiByte   | (E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = hiByte   & (~E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = loByte   | (E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = loByte   & (~E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
     // 	_io_ports[M6811_PORTG] = loByte   + E_HI + RS_HI;

		l++;
	}

	while( l < 16 ) // Blank the rest of the line
	{
		_io_ports[M6811_PORTG] = 0x10   | (E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = 0x10   & (~E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = 0x00    | (E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
		_io_ports[M6811_PORTG] = 0x00    & (~E_HI | RS_HI & ~RW_HI);
		delay_Nus(LCD_WAIT);
     // 	_io_ports[M6811_PORTG] = 0x00    + E_HI + RS_HI;
		l++;
	}


	return 1;

}

//-------------------------------------------------------------------------
/**
Beep the beeper at freq for time.  Smaller values of freq=higher frequencies.
freq is NOT frequency of the output, more accurately it is N/frequency where N
depends on the clock and subsytem setup bits PR in TMSK2
TODO This uses TCTN which is used by RPM.  Fix so that beep is independent of
TCTN
*/
void Beep(short freq, short time)
{

    _io_ports[M6811_TCTL2] = 0x00; // Disable TIC
    // Set port A3 for output
    _io_ports[1]=0x08;


    while(time-- >0)
    {
        _io_ports[M6811_PORTA]=0x08;// toggle positive
        delay_Nus (freq);
        _io_ports[M6811_PORTA]=0x00;// toggle neg
        delay_Nus (freq);
    }
    // Reset

    _io_ports[M6811_TCTL2] = 0x20; // Capture on falling edge

}

//-------------------------------------------------------------------------
/**
Read the keyboard and return the keycode.  Keyboard is a 1x4 matrix board.
and are mapped 1-1 to the lower 4 bits of port C in the PPI
Dual key presses can be detected for other control functions.
We use polled mode for this as this should be fast enough for our purposes.
Double presses and bounces will need to be catered for.  I guess we could use
the timer subsystem overflow flag to clear the keyboard pressed byte.  This
depends on the timer divider rate I guess.
TODO: There is no PPI on the latest board. It's direct to PD2-5
@param NULL
@return char or 0 if no key pressed
TODO double press and hold
*/
char readKeyBoard()
{
    //setup_PPI(0x88); // Write LCD and READ keyboard
    //key = _PPI[PPI_PORTC]; // PPI Port C input.  Upper nibble is key code
    key =  _io_ports[M6811_PORTD] & 0x3C;
    // right shift the key to get the key code in lower nibble
    key >>= 2;
    //key >>= 4;
    // Do some debouncing, double key press and held key repeat stuff.

    if(lastKey == 0x0F)
    {
        lastKey = key;
        return key;
    }

    if (key == lastKey) // If the same as previous press. ignore it as held key
    {
        return 0x0F;
    }

    lastKey = key;
    return 0x0f;


}

//-------------------------------------------------------------------------
/**
Monitor the serial line and respond
Default is to just echo the typed char.  Control chars as defined.
TODO Add code for setting of warning parameters etc.
exTempHi = 96,
engineTempHi = 65,
oilPressLo = 5,
*/
char monSerial()
{
    char buf[] = "    ";
    value_t val;
	int c='\0';
	if(serial_receive_pending ())c =  serial_recv();
	else return 0; // and do nothing else

    switch(c)
    {
    case 'b': // just beep
        serial_print("Beep!");
        Beep(5,200);
        serial_print("\n");
        break;

    case 'o': // Oil pressure
        val = getOilPress();
        dec_convert(buf, val);
        serial_print(buf);
       serial_print("\n");
	   //serial_send(val);
        break;

		case 'O': // Oil pressure warning
        val = oilPressLo;
        dec_convert(buf, val);
        serial_print(buf);
       serial_print("\n");
	   //serial_send(val);
        break;

    case 'e': // Exhaust temperature
        val = getExTemp();
        dec_convert(buf, val);
        serial_print(buf);
        serial_print("\n");
       //serial_send(val);
        break;

		case 'E': // Exhaust temperature warning
        val = exTempHi;
        dec_convert(buf, val);
        serial_print(buf);
        serial_print("\n");
       //serial_send(val);
        break;


    case 'w':// water temperature
        val = GetEngineTemperature();
        dec_convert(buf, val);
        serial_print(buf);
        serial_print("\n");
   //serial_send(val);
        break;

		case 'W': // Water temperature warning
        val = engineTempHi;
        dec_convert(buf, val);
        serial_print(buf);
        serial_print("\n");
       //serial_send(val);
        break;

    case 'r': // Send rpm
        val = rpm;
		dec_convert(buf, val);
        serial_print(buf);
		serial_print("\n");
		//serial_send(val);
        break;

	case 'H': // Engine hours(minutes)
        val = minutesRunCount;
		dec_convert(buf, val);
        serial_print(buf);
		serial_print("\n");
		//serial_send(val);
        break;


		case 's': // Send sea temperature
        val = seaTemp;
		dec_convert(buf, val);
        serial_print(buf);
		serial_print("\n");
		//serial_send(val);
        break;

	case 'x': // Set warning levels
		SetWarnings();
		break;

    case 13: // Return key
        serial_print("\n");
        break;

    default: // just echo
        serial_send(c);
		serial_print("\n");
        break;
    }

    return c;
}//monSerial()

//------------------------------------------------------------------------
/**
Set parameters from RS232 rx data from approved :-) software.  Sold separately
of course!
This is called from  monSerial() if an 'x' is received from the PC connection.
We acknowledge then wait for a sequence of non ASCII data, i.e. raw values which
are then stored in the correct place holders.
This will lock up the application until COP reset occurs if no data are received
*/
void SetWarnings()
{
	// Acknowledge by echoing a y
	//serial_print("y\n");
	char buf[10];
	// Wait till we get a line on serial connection.
	// THIS WILL WAIT FOREVER IF NO ARE RECEIVED !
	// Or until COP reset occurs
	serial_getline (buf);
	// Check value of first char to confirm this is data, bail out if not.
	if(buf[0] != 'z')return;
	// The rest are data values;

	cop_optional_reset();  //reset the COP each time
	asm volatile ("sei"); // Disable interrupts

	// Set exhaust warning temperature
	eeprom_write_byte (&exTempHi, buf[1]);

	// Set engine temperature warning
	eeprom_write_byte (&engineTempHi, buf[2]);

	// Set oil pressure warning
	eeprom_write_byte (&oilPressLo, buf[3]);

	//Set back light on or off
	eeprom_write_byte (& backLight, buf[4]);

	// Write user set engine run time
	eeprom_write_byte (&minutesRunCount, (buf[5]));
	eeprom_write_byte (&minutesRunCount+1, (buf[6]));
	// Define the presence of the rpm sensor
	//eeprom_write_byte (&rpmSensor, (buf[7]));

	// Set key beep on or off
	//eeprom_write_byte (&keyBeep, buf[8]);

	asm volatile ("cli"); // Enable interrupts

	serial_print("\n");

	Beep(2,100);

	return;
}

//------------------------------------------------------------------------
/**
Send logging info out the serial port to a laptop or summat.
Send simple ASCII strings for relevent values
*/
void SendSerial()
{
    char buf[] = "0123456789";
    value_t val;
    val = getOilPress();
    //serial_print("Oil pressure: ");
    dec_convert(buf, val);
    serial_print("Oil= ");
    serial_print(buf);
    serial_print(" psi\n\n\r");
    val = getExTemp();
    dec_convert(buf, val);
    serial_print("Exhaust= ");
    serial_print(buf);
    //serial_print("\r\n");
    serial_print(" �C\n\n\r");
    val = GetEngineTemperature();
    dec_convert(buf, val);
    serial_print("Water=");
    serial_print(buf);
    serial_print(" �C\n\n\r");

    val = rpm;
    dec_convert(buf, val);
    serial_print("RPM=");
    serial_print(buf);
    serial_print("\n\n\r");


}

//------------------------------------------------------------------------
/**
Get the engine oil pressure
This will involve interpreting some sort of sensor, getting an average, and
converting it to a human readable pressure reading.
Should also be in settable units I guess.
*/
char getOilPress()
{
    short sum = 0; // holds th sum of the reads

    char result,n = 10, a = n; // n, counter for averaging function

    while(n>0){
        result = read_analog(2); // Get the A/D value
        sum =+ result;
        n--;
    };

    oilPressure = sum / a;
    //oilPressure = 10; // debug
    if(oilPressure < oilPressLo && rpm > LOW_RPM )alarm |= ALARM_OIL;else alarm &= ~ALARM_OIL;
    // Store the oil pressure in the display buffer
    char buf[] = "  ";
    dec_convert(buf,oilPressure);
    char i=0;
    while(i++<2)
    {
        if(buf[i] != 0x0) // prevent writing End Of Line
            runStatus[0][i+11] = buf[i];
    }
    return oilPressure;
}

//-------------------------------------------------------------------------
/**
Get the exhaust temperature
*/
unsigned char getExTemp()
{

int result=0; // n, counter for averaging function
char n = 10;
    while(n>0){
        result += read_analog(1); // Get the A/D value
        n--;
    }
  result /= 10;
    // The simple approximation for a useable range of 70C to 150C with a
    // 820 ohm resistor

    // From spreadsheet: temp = -0.58*Rt + 51.06;
    // But 0.579 is approximately 4/7 so...
    unsigned char exTemp = (unsigned char)(-result * 4 / 7) + 198;
    //StoreExTemp(temperature);
    StoreExTemp(exTemp); // INto display array
    if(exTemp > exTempHi && rpm > LOW_RPM)alarm |= ALARM_EXHAUST;// Set the alarm if too high
    else alarm &= ~ALARM_EXHAUST;
    return exTemp;
}
//-------------------------------------------------------------------------
/**
Stores temp as text into screen buffer
runStatus[][17] = {
	{"1234 RPM   22PSI"},
    {"EX=123C  HD=123C"},
    {"1.2 AMPS CHARGE "},
    {"BATT 13.6 VOLTS "},
    {""},
*/
void StoreExTemp(char t)
{
    char buf[] = "   ";
    dec_convert(buf,t);
    int i=0;
    while(i<3)
    {
        if(buf[i] != 0x0)runStatus[1][i+3] = buf[i];
        else runStatus[1][i+3] = 0x20;
        i++;
    }

}
//-------------------------------------------------------------------------
/**
Get the engine water temperature.  Similar to getExTemp() but sensor is
linearised to a temperature range 50C to 90C by using a resistor value = 5K
*/
char GetEngineTemperature()
{
int result = 0; // n, counter for averaging function
char n = 10;
    while(n>0){
        result += read_analog(3); // Get the A/D value
        n--;
    }
	result /= 10;
    //int Rt = 5100 * (256/result);
    // From spreadsheet: temp = 0.4367 + 12.503
    // Now store in the strings we will display
    char  temperature;// = (char)((Rt * 10 / 23) + 12);
	temperature = (char)(-result * 7 / 16 ) + 124;
    StoreEngineTemp(temperature);
    // Set alram status
	if(temperature > engineTempHi && rpm > LOW_RPM)alarm |= ALARM_HEAD;// Set the alarm if too high
    else alarm &= ~ALARM_HEAD; // Zero the alarm bit
    return temperature;
}
//-------------------------------------------------------------------------
/**
Store the engine water temperature.
	{"1234 RPM   22PSI"},
    {"EX=123C  HD=123C"},
    {"1.2 AMPS CHARGE "},
    {"BATT 13.6 VOLTS "},
    {"1234 RPM HD=123C"},
*/
void StoreEngineTemp(char t)
{
    char buf[] = "   ";
    dec_convert(buf,t);
    int i=0;
    while(i<3)
    {
        if(buf[i] != 0x0){
            runStatus[1][i+12] = buf[i];

        }
        else {
            runStatus[1][i+12] = 0x20;
        }
        i++;
    }

}
//-------------------------------------------------------------------------
/**
Get the delta T from the fuel flow sensor.  Calculate fuel flow and store.

*/
void GetFuelFlow(void)
{
    char buf[] = "   ";
    /*
        dec_convert(buf,t);
        int i=0;
        while(i++<3)
        {
            if(buf[i] != 0x0)
    		{
                runStatus[4][i+6] = buf[i];

            }
     	}*/
}

//-------------------------------------------------------------------------
/**
Check if the cooling water inlet has excessive vacuum.  This can be from a
preset switch or a vacuum pressure gauge.  Just display alarm, no need
to display a value for this.  Digital input.
@return void
@param void
*/
void GetWaterVacuum()
{
    //if(0)alarm = ALARM_WATER;
}

//-------------------------------------------------------------------------
/**
Get the sea temperature.  Might be useful if you fancy a swim!
Can be from a sensor on the water intake fitting.  Else the wine rack!
*/
void GetSeaTemp(void)
{
int result=0; // n, counter for averaging function
char n = 10;
    while(n>0){
        result += read_analog(0); // Get the A/D value
        n--;
    }
  result /= 10;
    // The simple approximation for a useable range of 0C to 50C with a
    // 20K ohm resistor.  See spredsheet for details

    seaTemp = (unsigned char)(-result * 23 / 64) + 75;
    char buf[] = "   ";
    dec_convert(buf,seaTemp);
    n=0;
    while(n<3)
    {
        if(buf[n] != 0x0){
            runStatus[5][n+5] = buf[n];

        }
        else {
            runStatus[5][n+5] = 0x20;
        }
        n++;
    }

}

//-------------------------------------------------------------------------
/**
wait for indicated time in ms.  If 10ms = 40000 then...
N < 16 alse will overflow
*/
void delay_Nms (short N)
{
    uint16_t tmp = 4000*N;
    asm volatile ("1: subd #1\n"
              "	bne 1b" : "=d"(tmp) : "d"(tmp));

}

//-------------------------------------------------------------------------
/**
wait for indicated time in us.  If 10ms = 40000 then...
Really should use hardware timeer counter.
*/
void delay_Nus (short N)
{
    uint16_t tmp = 4*N;
    asm volatile ("1: subd #1\n"
              "	bne 1b" : "=d"(tmp) : "d"(tmp));

}

//-------------------------------------------------------------------------
/**
wait for indicated time in s.
*/
void delay_s (short N)
{
	if(N > 59)return; // No minutes here!
	char key = readKeyBoard();
    char now = ticks;
    if(now + N >59 )now =-60;
    while(now > ticks && key != KEY_LEFT)
	{
		key = readKeyBoard();
	}// loop
    return;
}
//-------------------------------------------------------------------------
/**
Display the engine run time in hours.  Not to be confused with RunDisplay()
*/
void DisplayRunTime()
{
    char  buf[] = "           "; //Allocate storage for buffer
    unsigned int hours = minutesRunCount/60;
    dec_convert(buf, hours);
    lcdWrite("ENGINE HOURS", 1);
    lcdWrite(buf, 2);

}
//-------------------------------------------------------------------------
/**
Return the highest status alarm bit position.  Actually this is lowest bit since
alarm priority runs from 1 = most important to 8 least important
*/
unsigned char getHighestAlarm(unsigned char alarm)
{
    unsigned char al = 8;
    while(alarm != 0)
    {
        alarm <<= 1;
        al--;
    }
    return al;
}
//-------------------------------------------------------------------------
/**
Do the normal run display updating with fresh values and current display line.
*/
void RunDisplay()
{
    // Display any warnings
    if(alarm != 0 )
    {
        lcdWrite("!!  WARNING   !!",1);
        lcdWrite(warning[getHighestAlarm(alarm)],2);
        return;
    }
    //If no warning then...

    if(runmode == 1) // Normal engine running mode
    {
        lcdWrite(runStatus[statusLine],1);
        lcdWrite(runStatus[statusLine + 1], 2);
    }
    if(runmode == 2) // Count up timer mode
    {
        lcdWrite("TIMER",1);
        lcdWrite(time,2);
    }
    if(runmode == 3) // Engine hours display
    {
        DisplayRunTime();
    }

}
//-------------------------------------------------------------------------
/**
Start the minute timer
*/
void StartMinuteTimer(char minutes)
{
}
//-------------------------------------------------------------------------
/**
Blow a fog horn at correct but varying intervals
*/
void BlowFogHorn(void)
{
}
//-------------------------------------------------------------------------
/**
If the EPROM version does not match the version number in EEPROM then there may
be variables/constants that need to set in EEPROM.  This puts correct values
into EEPROM.  Only runs once on insertion of new EPROM if version different.
NOT the same as code version.
Intended for use with "in the field" upgrades when connection to a computer
is not feasible.

Example code for ref
extern char _etext, _data, _edata, _bstart, _bend;
char *src = &_etext;
char *dst = &_data;


while (dst < &_edata) {
eeprom_write...
  *dst++ = *src++;
}

*/
void UpdateEEPROM(void)
{
    serial_print("Updating EEPROM\n\r");
    lcdWrite("Updating EEPROM", 1);
    //asm volatile ("sei");
    extern unsigned int  _start_vectors, _end_eeprom_vars, _start_eeprom_vars,
        __load_start_vectors, __load_start_eeprom_vars ;


    char *sr = (char*)&__load_start_vectors;
    char *ds = (char*)&_start_vectors;


    // ROM has data at end of text; copy it

    // Copy vectors
    while (ds < 0xffff)
    {
		// Beep(5,40);
        //lcdWrite(*sr, 2);
        eeprom_write_byte (ds, *sr);
        *ds++;*sr++;

    }

	cop_optional_reset();

	// copy "mutable constants"
    char *src = (char*)&__load_start_eeprom_vars; // *src = the address of __load_start_eeprom_vars
    char *dst = (char*)&_start_eeprom_vars;

    while (dst < (char*)&_end_eeprom_vars )
    {
        eeprom_write_byte (dst, *src);
        *src++;*dst++;
		//lcdWrite(*src, 2);
		//Beep(5,40);
    }

    // Update the version number
    eeprom_write_byte(&version, VERSION);
	//asm volatile ("cli");
    lcdWrite("EEPROM updated", 1);
    serial_print("EEPROM updated\n\r\n");
	Beep(10,40);
    delay_s (2);


}

//-------------------------------------------------------------------------
/**
Set up the Exhaust temperature calibration curve.  We get here vea a double key
press, probably UP+DOWN.
We ask the user to set certain conditions, RPM load/noload, and then hit a key
to save the data.  This forms the basis of a crude normal temperature curve
that is the baseline for warning values.  Some problems occur with rapid
slow down as the sensor is still hot while the RPM would have fallen and so the
curve will be wrong unless some hysteresis in built in to allow for this.  The
ref RPM value could be a geometric time progression from old to new value.
Would have to monitor to find out.  More data please.
*/
void TCurveCal()
{
	return;
    Beep(10,100);
    char stage=0;
    char exT[10];
    char key; // must be outside loop
    do{
        key = readKeyBoard();
        if(stage ==0)
        {

            lcdWrite("SET ENGINE IDLE",1);
            lcdWrite("PRESS UP KEY",2);
            //Wait here for sensor to equilibriate
			delay_s (5);

        }
        if(stage ==1)
        {

            lcdWrite("SET ENGINE MIDDLE",1);
            lcdWrite("PRESS UP KEY",2);
            //Wait here for sensor to equilibriate
			delay_s (5);
        }
        if(stage ==2)
        {

            lcdWrite("SET ENGINE FULL",1);
            lcdWrite("PRESS UP KEY",2);
            //Wait here for sensor to equilibriate
			delay_s (5);
            while(exT[0] > exT[1]-3 && exT[0] < exT[1]+3 && exT[1] != T[stage-1])
            {
                exT[0] = exT[1];
                delay_s (5);
                exT[1] = exTemp;
            }
        }
        if(key == KEY_UP)//Read RPM & ExT and store
        {
            Beep(10,50);
            RPM[stage] = rpm;
            T[stage] = exTemp;
            stage++;
            if(stage == 3)return;// Done
        }
        if (key == 0x0f)continue;
        //if(keyBeep)Beep(10,50);
    }while(key != KEY_LEFT);// To escape
}

//-------------------------------------------------------------------------
/**
Main function starts here
*/
int main()
{
    asm volatile ("sei");
    LCD_init();
    if(version != VERSION)
    UpdateEEPROM();
    flush ();
    serial_init ();
    serial_flush ();
    ticks = 0; // Zero the timer
    runmode = 1;

	// Enable interrupts
    asm volatile ("cli");
    Beep(5,500);
    serial_print("\n\rYacht Engine Monitor.\n\r");
    serial_print(revision);
    serial_print("\n\n\r");
    lcdWrite("Engine Monitor", 1);
    lcdWrite(revision, 2);
    delay_Nms (1000);delay_Nms (1000);delay_Nms (1000);delay_Nms (1000);
    rpm = 0;
    // Start RPM stuff by:
    // Set Timer Interrupt Mask Register 1  Rising edge of TIC1 for rev counter
    // and set interrupt enable
    _io_ports[M6811_TCTL2] = 0x20; // Capture on falling edge
    _io_ports[M6811_TMSK1] = 0x04;
    _io_ports[M6811_TFLG1] = 0x04; 	/* Clear Timer Interrupt Flag Register 1 */
    _io_ports[M6811_HPRIO] = 0x00; // Elevate TOF to highest priority - overflow
    //_io_ports[M6811_HPRIO] = 0x07; // Elevate rti = clock
    //_io_ports[M6811_HPRIO] = 0x08; // Elevate tic1 to highest priority - rpm
    _io_ports[M6811_TFLG2] = 0xFF; // Clear all timer flags, enabled in _premain
    _io_ports[M6811_PACTL] |= 0x02; // Set RTR[0:1]
    //_io_ports[M6811_OPTION] |= 0x40; // Use RC for charge pump

    //eeprom_write_byte ((char*)0xFED0, 0xFD);
    Beep(25,75); // Start up beep

    while(monitor)monitor();
    _exit(0);
}
//-------------------------------------------------------------------------


//-------------------------------------------------------------------------
/*
$Log: EngMon.c,v $
Removed FlashDog()

Revision 1.33  2004/05/13 20:59:44  mikee
Changed timer to 4us as it should always have been!

Revision 1.32  2004/05/10 17:28:13  mikee
Done.  Release-1-7

Revision 1.31  2004/05/07 10:05:38  mikee
Working

Revision 1.30  2004/03/25 23:07:54  mikee
Temperture readings and alarms working.  Cursor off in lcdWrite().  EEPROM update fixed.

Revision 1.29  2004/03/22 23:40:19  mikee
EEPROM_update now working.

Revision 1.28  2004/02/02 22:47:40  mikee
EEPROM write working.  Interrupts HAVE to be disabled.
Somehow no return from SetAlarms() occuring.  Suspect stuck
in loop but don't know why exactly.

Revision 1.27  2004/02/01 10:58:17  mikee
Almost OK.  eeprom_write appears to be broken.

Revision 1.26  2004/01/29 20:17:01  mikee
ckup

Revision 1.25  2004/01/28 21:52:57  mikee
EPROM version func added

Revision 1.24  2004/01/28 00:18:46  mikee
update

Revision 1.23  2004/01/22 22:49:40  mikee
L&R keypress fixed

Revision 1.22  2004/01/22 21:05:24  mikee
Many changes relating to rpm.  Beep() changed to use delay() method

Revision 1.21  2003/12/17 17:37:48  mikee
Changed memory mapping.  intram -> data

Revision 1.20  2003/12/16 23:03:53  mikee
Timer working using real time interrupt.  Dec_convert fixed to right justify
the text in the buffer.

Revision 1.19  2003/12/02 19:15:35  mikee
Added RPM stuff

Revision 1.18  2003/12/02 09:14:18  mikee
Backup

Revision 1.17  2003/12/01 23:46:41  mikee
Added timer functions

Revision 1.16  2003/11/26 21:43:33  mikee
Screen working.  Exhaust T working = A/D input.  Key functions added.

Revision 1.15  2003/11/25 08:38:26  mikee
Backup

Revision 1.14  2003/11/19 00:00:58  mikee
Backup

Revision 1.13  2003/11/18 21:53:14  mikee
Some keyboard functions working, e.g. parameter setup.
Key mapping changed and may well do again!

Revision 1.12  2003/11/18 00:48:36  mikee
Setup warnings extended

Revision 1.11  2003/11/17 21:54:11  mikee
Setup warnings added

Revision 1.10  2003/11/12 23:49:30  mikee
Added TOF ISR.  Added vectors then moved these to new header file EngMon.h.

Revision 1.9  2003/11/11 19:35:59  mikee
Removed _start()

Revision 1.8  2003/11/11 19:35:21  mikee
Added __premain()

Revision 1.7  2003/10/15 19:26:08  mikee
Backup

Revision 1.6  2003/09/24 22:50:37  mikee
Backup

Revision 1.4  2003/09/18 14:41:30  mikee
Backup

Revision 1.3  2003/09/17 22:11:59  mikee
Temperature works
Also variables and constants are in the correct memory area

Revision 1.2  2003/09/16 19:45:56  mikee
Beeping

*/
//-------------------------------------------------------------------------

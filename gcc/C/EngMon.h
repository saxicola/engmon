//-------------------------------------------------------------------------
/*
File: EngMon.h
Header file for Engine monitor file: simple.c

$Author: mikee $
$Id: EngMon.h,v 1.11 2004/05/07 10:06:07 mikee Exp $
*/
//-------------------------------------------------------------------------

#include <stdarg.h>
#include <gel/var.h>


typedef short value_t;
typedef char value_s;


/* Function prototypes */
//void _start();
void __premain(void);
char read_analog(char port);
char monitor(void);
char lcdWrite(char*,char);
char readKeyBoard(void);
void doKeyboard(char key);
void Beep(short freq, short time);
void setup_PPI(char);
void LCD_init(void);
void delay_Nms (short N);
void delay_Nus (short N);
void delay_s (short N);
static char* hex_convert(char* buf, value_t value);
static char* dec_convert(char* buf, value_t value);
char monSerial(void);
char getOilPress(void);
unsigned char getExTemp(void);
void StoreExTemp(char temperature);
char GetEngineTemperature(void);
void StoreEngineTemp(char t);
static void flush(void);
void DisplayRunTime(void);
char SetAlarms(void);
void RunDisplay(void);
void StartMinuteTimer(char minutes);
void BlowFogHorn(void);
void setRPM(void);
void SendSerial(void);
unsigned char getHighestAlarm(unsigned char);
void UpdateEEPROM(void);
void GetFuelFlow(void);
void GetWaterVacuum(void);
void GetSeaTemp(void);
void TCurveCal(void);
void SetWarnings(void);

/* Interrupt function prototyps */ 
void cop_fail_interrupt(void) __attribute__((interrupt));
void rpm_interrupt(void) __attribute__((interrupt));
void timer_overflow_interrupt(void) __attribute__((interrupt));
void rti_handler(void)  __attribute__((interrupt));
//void toc1_interrupt(void) __attribute__((interrupt)); 


 

//-------------------------------------------------------------------------
/*
$Log: EngMon.h,v $
Revision 1.11  2004/05/07 10:06:07  mikee
Working

Revision 1.10  2004/03/25 23:07:54  mikee
Temperture readings and alarms working.  Cursor off in lcdWrite().  EEPROM update fixed.

*/

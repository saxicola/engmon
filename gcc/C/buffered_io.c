/*
   Copyright (C) 2001 Free Software Foundation, Inc.
   Written by Duane Gustavus (duane@denton.com)

This file is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

In addition to the permissions in the GNU General Public License, the
Free Software Foundation gives you unlimited permission to link the
compiled version of this file with other programs, and to distribute
those programs without any restriction coming from the use of this
file.  (The General Public License restrictions do apply in other
respects; for example, they cover modification of the file, and
distribution when not linked into another program.)

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.  */

/*
 * buffered_io.c provides interrupt driven serial I/O using SCI
 *
 */

#include <sys/param.h>
#include <sys/ports.h>
#include <sys/interrupts.h>
#include <test.h>

/* buffers and index variables for stdin and stdout */
#define BUFLEN 82
static unsigned char sci_inbuf[BUFLEN];
static unsigned char sci_in_head, sci_in_tail;
static unsigned char sci_outbuf[BUFLEN];
static unsigned char sci_out_head, sci_out_tail;
static unsigned char sci_status;

/* SCI interrupt service routine */
void sci_service(void)  __attribute__((interrupt));
void
sci_service(void)
{
    /* read SCSR to determine required service */
    sci_status = _io_ports[M6811_SCSR];
    /* receiver data available? */
    if (sci_status & M6811_RDRF) {
        /* read SCDR */
        sci_inbuf[sci_in_head] = _io_ports[M6811_SCDR];
        /* increment index modulo BUFLEN */
        if((sci_in_head += 1) == BUFLEN) { sci_in_head = 0; }
        if (sci_in_head == sci_in_tail) {
            /* no room in buffer; decr index */
            if ((sci_in_head -=1) >= BUFLEN) { sci_in_head = BUFLEN -1; }
        }
    }
    /* transmitter buffer empty? */
    if (sci_status & M6811_TDRE) {
        /* anything to transmit? */
        if (sci_out_head == sci_out_tail) {
            /* buffer empty so turn off TDRE interrupt */
            _io_ports[M6811_SCCR2] &= ~0x80;
        } else {
            /* write SCDR */
            _io_ports[M6811_SCDR] = sci_outbuf[sci_out_tail];
            if ((sci_out_tail += 1) == BUFLEN) { sci_out_tail = 0; }
        }
    }
}

/* put next char in SCI output buffer */
unsigned char
sci_putc(char next)
{
    static unsigned char tmp;

    tmp = sci_out_head;
    /* increment tail index */
    if ((sci_out_head += 1) == BUFLEN) { sci_out_head = 0; }
    if (sci_out_tail == sci_out_head) {
        /*no room in buffer */
        sci_out_head = tmp;
        tmp = -1;
    } else {
        /* write char to buffer */
        sci_outbuf[tmp] = next;
        tmp = 0;
    }
    /* turn on TDRE interrupts */
    _io_ports[M6811_SCCR2] |= 0x80;
    return(tmp);
}

/* print string using buffered output; if buffer overflows, chars
 * are dropped.  Returns number of chars actually printed.
 */
int
serial_print(char *string)
{
    static int count;

    count = 0;
    /* while pointer is not null (end of string) */
    while (*string != '\0') {
        /* abort if buffer is full */
        while (sci_putc(*string)) { break; }
        /* increment count of chars printed and string pointer */
        string += 1; count += 1;
    }
    return(count);
}

/* get a char from the SCI input buffer -- BLOCKS!! */
unsigned char
sci_getc()
{
    static unsigned char token;

    while (sci_in_head == sci_in_tail) { /* wait for input */ }
    token = sci_inbuf[sci_in_tail];
    if ((sci_in_tail += 1) == BUFLEN) { sci_in_tail = 0; }
    return (token);
}

/* return number of chars available in sci_inbuf */
inline unsigned char
chk_serial_inbuf(void)
{
    return sci_in_head - sci_in_tail;
}

/* initialize SCI hardware and I/O buffers */
void
init_buffered_io()
{
    /* Set SCI interrupt handler */
    set_interrupt_handler(SCI_VECTOR, sci_service);
    /* init buffer pointers */
    sci_in_head = sci_in_tail = 0;
    sci_out_head = sci_out_tail = 0;
    /* setup baud rate, number bits for SCI -- 48 for 9600 baud */
    _io_ports[M6811_BAUD] = 48;
    _io_ports[M6811_SCCR1] = 0;
    /* enable SCI transmitter and receiver and RIE */
    _io_ports[M6811_SCCR2] |= 0x2c;
}

#!/bin/sh
# Print out the addresses of the functions sorted by hardware address
FILE=$1

#m6811-elf-readelf -a simple.elf | grep FUNC | sort --key=2
m6811-elf-readelf -s  $FILE | sort --key=2

#$Log: func-list.sh,v $
#Revision 1.4  2004/03/23 00:00:56  mikee
#Added Log
#

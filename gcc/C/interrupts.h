/* Interrupt table for 68HC11
   Note: the `XXX_handler: foo' notation is a GNU extension which is
   used here to ensure correct association of the handler in the struct.
   This is why the order of handlers declared below does not follow
   the HC11 order.  */
#include <sys/interrupts.h>

#ifdef USE_INTERRUPT_TABLE

#define fatal_interrupt 0x8000 // Force a reset



struct interrupt_vectors __attribute__((section(".vectors"))) vectors =
{
  res0_handler:           fatal_interrupt, /* res0 */
  res1_handler:           fatal_interrupt,
  res2_handler:           fatal_interrupt,
  res3_handler:           fatal_interrupt,
  res4_handler:           fatal_interrupt,
  res5_handler:           fatal_interrupt,
  res6_handler:           fatal_interrupt,
  res7_handler:           fatal_interrupt,
  res8_handler:           fatal_interrupt,
  res9_handler:           fatal_interrupt,
  res10_handler:          fatal_interrupt, /* res 10 */
  sci_handler:            fatal_interrupt, /* sci */
  spi_handler:            fatal_interrupt, /* spi */
  acc_overflow_handler:   fatal_interrupt, /* acc overflow */
  acc_input_handler:      fatal_interrupt,
  timer_overflow_handler: timer_overflow_interrupt,
  output5_handler:        fatal_interrupt, /* out compare 5 */
  output4_handler:        fatal_interrupt, /* out compare 4 */
  output3_handler:        fatal_interrupt, /* out compare 3 */
  output2_handler:        fatal_interrupt, /* out compare 2 */
//  output1_handler:        toc1_interrupt, /* out compare 1 */
  output1_handler:        fatal_interrupt, /* out compare 1 */
  capture3_handler:       rpm_interrupt, /* in capt 3 */
  capture2_handler:       rpm_interrupt, /* in capt 2 */
  capture1_handler:       rpm_interrupt, /* in capt 1 */
  irq_handler:            fatal_interrupt, /* IRQ */
  xirq_handler:           fatal_interrupt, /* XIRQ */
  swi_handler:            fatal_interrupt, /* swi */
  illegal_handler:        fatal_interrupt, /* illegal */
  cop_fail_handler:       cop_fail_interrupt,
  cop_clock_handler:      fatal_interrupt,
  rtii_handler:           rti_handler, //timer_interrupt,
  reset_handler:          _start
};

#endif

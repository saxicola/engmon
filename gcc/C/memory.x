/*
memory.x file for MC68HC11 micro.  Variants can be programmed.  See Log, CVS
$Id: memory.x,v 1.12 2004/05/07 10:05:52 mikee Exp $
*/

MEMORY
{
page0  (rwx) : ORIGIN = 0x0, LENGTH = 0x100
bss	(rw)	: ORIGIN = 0x100, LENGTH = 0x100
text  (rx)  : ORIGIN = 0x8000, LENGTH = 0x7E00 /* All below EEPROM in the F1*/
eeprom (rx)  : ORIGIN = 0xFE00, LENGTH = 0x1C0 /* In F1 chip */
data   (rw)  : ORIGIN = 0x0100, LENGTH = 0x0100
vectors(rw)  : ORIGIN = 0xffC0, LENGTH = 0x0040
}

/* Setup the stack on the top of the data memory bank.  */
PROVIDE (_stack = 0x0400 - 1); /* Set to top of internal RAM */
PROVIDE (_BASE = 0x00); /* This is defined as extern volatile unsigned char _BASE[] in program */
PROVIDE (_io_ports = 0x1000); /* The register base */
PROVIDE	(_PPI = 0x6000); /* PPI chip memory mapping*/





SECTIONS
  {

  .Mvectors 0xffc0 :
    AT (__data_image_end)
    { _start_vectors = . ; *(.vectors); _end_vectors = . ;  }
        __load_start_vectors =  LOADADDR (.Mvectors);
        __load_end_vectors =  LOADADDR (.Mvectors) + SIZEOF (.Mvectors);

  .Meeprom 0xFE00 :
    AT ( (__load_end_vectors) )
    { _start_eeprom_vars = . ; *(.eeprom); _end_eeprom_vars = . ;  }
        __load_start_eeprom_vars =  LOADADDR (.Meeprom);
        __load_end_eeprom_vars =  LOADADDR (.Meeprom) + SIZEOF (.Meeprom);

}





/*
$Log: memory.x,v $
Revision 1.12  2004/05/07 10:05:52  mikee
Fiddling about

Revision 1.11  2004/02/16 20:05:25  mikee
Configured for EPROM compatability

Revision 1.10  2004/02/02 22:48:00  mikee
Working backup

Revision 1.9  2004/02/01 15:43:28  mikee
Some obsolete stuff removed

Revision 1.8  2004/02/01 10:58:17  mikee
Almost OK.  eeprom_write appears to be broken.

Revision 1.7  2004/01/28 00:20:08  mikee
update

Revision 1.6  2003/11/17 21:52:52  mikee
Backup

Revision 1.5  2003/10/30 18:45:23  mikee
Minor edits

Revision 1.4  2003/10/30 18:43:34  mikee
Revert back to F1 chip.  PCB in production

Revision 1.3  2003/10/27 19:46:21  mikee
E1 memory mapping defined

*/

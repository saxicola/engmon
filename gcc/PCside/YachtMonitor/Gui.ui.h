/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
** $Id: Gui.ui.h,v 1.2 2003/10/15 20:48:10 mikee Exp $
*****************************************************************************/
#include <qvalidator.h> 
#include <stdio.h>
#include "RS232comm.h"
#include <qmessagebox.h> 
#include <time.h>
#include <qthread.h>
#include <qobject.h> 
#include <qtimer.h> 
//---------------------------------------------------------------------------------------------------------------------------------
int ExHiAlarm = 95; //C
int WaterHiAlarm = 95; //C
int OilLowAlarm = 5; //psi
bool collecting = false;
char *  port = "/dev/ttyS1"; // ttyS1 or ttyS2
static FILE * ofp;
int wait = 2000; //Timed data collection in mseconds
QTimer *timer = new QTimer();
char inbuf[255];  
timespec req,* rem;  

//---------------------------------------------------------------------------------------------------------------------------------
/**
 A class to run as a timed process to run concurrently with main 
*
class MyThread : public QThread {

    public:

        virtual void run();

    };

//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
void GUI::init()
{
	
	EditExHi->setValidator(new QIntValidator(0,255,EditExHi));
	EditWaterHi->setValidator(new QIntValidator(0,255,EditWaterHi));
	EditOilLo->setValidator(new QIntValidator(0,255,EditOilLo));
	PortSet();
	connect( timer, SIGNAL(timeout()), SLOT(GetTimedData()) ); 
  
	//MyThread a;
        
	
}
//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
void GUI::destroy()
{
	stopCollection();
}

//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
void GUI::ExHiSet()
{
	bool ok;
	//implement some code here
	QString text = EditExHi->displayText();
	ExHiAlarm=text.toInt(&ok,10);
	//if(ok == false)return;
	EditExTemp->setText(text);
	
}
//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
void GUI::WaterHiSet()
{
	bool ok;
	QString text = EditWaterHi->displayText();
	WaterHiAlarm = text.toInt(&ok,10);
	EditWaterTemp->setText(text);
}
//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
void GUI::OilLowSet()
{
	bool ok;
	QString text = EditOilLo->displayText();
	OilLowAlarm = text.toInt(&ok,10);
	EditOilPress->setText(text);
	
}

/** 
Start collecting data from the monitor by sending a control char to the monitor 
and parsing the response.
Display most recent data in GUI boxes.  Will need a timer of some sort
*/
void GUI::startDataCollection()
{	
	
	
	//a.start();
	if(collecting) return; //already collecting
	qDebug( "Starting Data Collection" );
	collecting = true;
	comboBoxPort->setEnabled(false);	
	int res;
	
	int stat = openPort(port);
	if (stat == 0)
	{
		QMessageBox::information( this, "Monitor", "PortOpenError" );
		return;
	}	
	
	
	
	
//return;
	//ofp=fopen("DataFile.csv","a");
	//fprintf(ofp,"\n\nNew Capture\n");
	QMessageBox::information( this, "Monitor", "Starting" );
		GetTimedData(); // then do it timed
		
	timer->start( wait, false); // 2 seconds 

	//};
	
}

//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
void GUI::stopCollection()
{
	collecting = false;
	timer->stop();
	//fclose(ofp);
	closePort(); 
	comboBoxPort->setEnabled(true);	
}

//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
void GUI::PortSet()
{
	int p = comboBoxPort->currentItem();
	if(p==0)port = "/dev/ttyS0";
	if(p==1)port = "/dev/ttyS1";
	//QMessageBox::information( this, "Monitor", port );
}

//---------------------------------------------------------------------------------------------------------------------------------
/**
 Do the actual data collection, called by a timer function
*/
void GUI::GetTimedData()
{
	SetHandler();
	qDebug( "Getting data" );
	
	sendData("b");
	//sendData("w");
	
	//int res = read(fd,inbuf,128);
	inbuf[res]=0;
	qDebug(inbuf);
	
	
}
//---------------------------------------------------------------------------------------------------------------------------------
/**
*/
void GUI::SetSampleTime()
{
	if(spinBoxTime->value() == 0) wait = 2000; // Default of  2 s
else wait = 1000* spinBoxTime->value();
}
//---------------------------------------------------------------------------------------------------------------------------------
/**
*/
void GUI::newSlot()
{

}
//---------------------------------------------------------------------------------------------------------------------------------
/**
  $Log: Gui.ui.h,v $
  Revision 1.2  2003/10/15 20:48:10  mikee
  Backup

*/

/****************************************************************************

** $Id: rs_comm.c,v 1.2 2003/10/15 20:48:24 mikee Exp $
*****************************************************************************/

 
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <errno.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
 

#define BAUDRATE B9600
//#define DEVICE "/dev/ttyS0" //COM1
//#define DEVICE "/dev/ttyS1" //COM2
#define _POSIX_SOURCE 1         //POSIX compliant source
#define FALSE 0
#define TRUE 1

volatile int STOP=FALSE;
void signal_handler_poo(int status);
char sendData(char * string);
void signal_handler_IO (int status);    //definition of signal handler
//void signal_handler_quit(int status);	//Signal handler for ^C
struct termios oldtio,  options;        //place for old and new port settings for serial port

struct sigaction saio;                  //definition of signal action
char inbuf[255];
//static char **data ;				    //buffer for where data are put
static int fd, res, error;
//static FILE *ifp1, *ifp2,*ofp;
//static char newest_test[10];
//static char config[100];
void closePort();
int SetHandler();
/**********************************************************************************
Open the port
**********************************************************************************/
int openPort(char * DEVICE)
{
	// Open the port read/write, not controlling tty, non blocking
	fd = open(DEVICE, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0)
	{
		perror(DEVICE);
		exit(-1);
	}
    // Get old settings, to be restored later on close
	tcgetattr(fd,&oldtio);




      // Make the file descriptor asynchronous (the manual page says only
      // O_APPEND and O_NONBLOCK, will work with F_SETFL...)
    //fcntl(fd, F_SETFL, FASYNC);
	fcntl(fd, F_SETFL, O_ASYNC);


	//Install a signal handler for ^C
	//signal(SIGQUIT, &sigquit, NULL);



    // Set the baud rate for input and output
	cfsetispeed(&options,BAUDRATE);	
	cfsetospeed(&options,BAUDRATE);	
	options.c_cflag |= (CLOCAL|CREAD);
    //tcsetattr(fd, TCSANOW, &options);


    //Set 8N1 no parity
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag |= CS8;

    // Set hardware flow control
	//options.c_cflag |= CRTSCTS;

	// Set software flow control
	options.c_cflag |= (IXON| IXOFF| IXANY);

    // Ignore parity and convert CR to LF
	//options.c_iflag |= IGNPAR|ICRNL;

    //Set canonical input, so get an proper line of text
	options.c_lflag |= (ICANON|ECHOE);  // No echo else get a loop
  //  options.c_lflag |= (ICANON | ECHO|ECHOE);

    //Set input parity checking and strip parity bits
	//options.c_iflag |= (INPCK | ISTRIP);

    //Set processed output
	options.c_oflag |= OPOST;

    // Set the options
	tcsetattr(fd,TCSAFLUSH,&options);
	
	
	
	return 1; // On success
}

/***********************************************************************************
Signal handler, gets the data from the buffer
***********************************************************************************/
void signal_handler_IO (int status)
{
	
	
	printf("Stuff Arrived\n");
	// Read from the port buffer
	res = read(fd,inbuf,128);
	
	// Set end of string so we can use printf
	//inbuf[res]='\0';
	printf("%s\n", inbuf);
	
	// Parse the data string into a string array
	//parseData(inbuf);
	//insertData();
	// Send a cflf back
	//c = write(fd,"\r", 1);
	
	// If 'z' then close and exit;
	//    if (inbuf[0]=='z')closePort();
	//return;
}
//---------------------------------------------------------------------------------------------------------------------------------
/**
  
*/
int SetHandler()
{
printf("Setting Handler\n");
	//install the serial handler before making the device asynchronous
	saio.sa_handler = signal_handler_IO;
	//saio.sa_handler = signal_handler_poo;
	sigemptyset(&saio.sa_mask);   //saio.sa_mask = 0;
	saio.sa_restorer = NULL; 
	saio.sa_flags = SA_NOMASK;  //Don't mask the calling signal

	sigaction(SIGIO,&saio,NULL);
	// allow the process to receive SIGIO
	fcntl(fd, F_SETOWN, getpid());
return 1;
}


/**********************************************************************************
unsetHandler
***********************************************************************************/
unsetHandler()
{
saio.sa_handler = signal_handler_poo;
sigaction(SIGIO,&saio,NULL);
printf("Unsetting handler\n");
}


/**********************************************************************************
Close Port
***********************************************************************************/
void closePort()
{
	// Restore old settings
	if(fd){
		tcsetattr(fd, TCSANOW,&oldtio); 
		close(fd);
	}
	return;	
}
/*********************************************************************************Send some data out the port
********************************************************************************/
void signal_handler_poo(int stuff)
{
	//char  c = write(fd, string, strlen(string));
	//write(fd, "\r", 2);
	//return c;
}
//-------------------------------------------------------------------------------
main()
{
char  * string = "b";

openPort("/dev/ttyS1");
SetHandler();
while(1){
//unsetHandler();
//SetHandler();
char  c = write(fd, string, 1);
//usleep(1000000); // Delay in microseconds
//res = read(fd,inbuf,128);
//SetHandler();
//usleep(1000000); // Delay in microseconds
pause();
}; //loop

}
//--------------------------------------------------
/**
  $Log: rs_comm.c,v $
  Revision 1.2  2003/10/15 20:48:24  mikee
  Backup

*/

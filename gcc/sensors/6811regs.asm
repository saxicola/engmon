
; file of standard 6811 register declarations.  Use as .include
	.text
	.nolist
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;$Id: 6811regs.asm,v 1.1.1.1 2003/09/15 12:17:40 mikee Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Control Registers

REGS	=	0x1000

PORTA	=	0x00	; Port A data register
DDRA	=	0x01	; Reserved
PORTG	=	0x02	; Parallel I/O Control register
DDRG	=	0x03	; Port C latched data register
PORTB	=	0x04	; Port B data register
PORTF	=	0x05	;
PORTC	=	0x06	; 
DDRC	=	0x07	; Data Direction register for port C
PORTD   = 	0x08	; Port D data register
DDRD    =	0x09	; Data Direction register for port D
PORTE	=	0x0A	; Port E data register
CFORC	=	0x0B	; Timer Compare Force Register
OC1M	=	0x0C	; Output Compare 1 Mask register
OC1D	=	0x0D	; Output Compare 1 Data register





; Some flag defines
RDRF		=	0x20	; SCSR "Received Data Ready" flag

;Some extras
CSSTRH		=	0x5C	; Clock Stretching
CSCTL		=	0x5D	; Chip Select Control
CSGADR		=	0x5E	; General Purpose Chip Select Address Register
CSGSIZ		=	0x5F	; General Purpose Chip Select Size Register
TDRE		=	0x80	; SCSR "Transmit Data Register Empty" flag



; Two-Byte Registers (High,Low -- Use Load & Store Double to access)
TCNT	=	0x0E	; Timer Count Register
TIC1	=	0x10	; Timer Input Capture register 1
TIC2	=	0x12	; Timer Input Capture register 2
TIC3	=	0x14	; Timer Input Capture register 3
TOC1	=	0x16	; Timer Output Compare register 1
TOC2	=	0x18	; Timer Output Compare register 2
TOC3	=	0x1A	; Timer Output Compare register 3
TOC4	=	0x1C	; Timer Output Compare register 4
TI4O5	=	0x1E	; Timer Input compare 4 or Output compare 5 register
TCTL1	=	0x20	; Timer Control register 1
TCTL2	=	0x21	; Timer Control register 2
TMSK1	=	0x22	; main Timer interrupt Mask register 1
TFLG1	=	0x23	; main Timer interrupt Flag register 1
TMSK2	=	0x24	; misc Timer interrupt Mask register 2
TFLG2	=	0x25	; misc Timer interrupt Flag register 2
PACTL	=	0x26	; Pulse Accumulator Control register
PACNT	=	0x27	; Pulse Accumulator Count register
SPCR	=	0x28	; SPI Control Register
SPSR	=	0x29	; SPI Status Register
SPDR	=	0x2A	; SPI Data Register
BAUD	=	0x2B	; SCI Baud Rate Control Register
SCCR1	=	0x2C	; SCI Control Register 1
SCCR2	=	0x2D	; SCI Control Register 2
SCSR	=	0x2E	; SCI Status Register
SCDR	=	0x2F	; SCI Data Register
ADCTL	=	0x30	; A/D Control/status Register
ADR1	=	0x31	; A/D Result Register 1
ADR2	=	0x32	; A/D Result Register 2
ADR3	=	0x33	; A/D Result Register 3
ADR4	=	0x34	; A/D Result Register 4
BPROT	=	0x35	; Block Protect register
RESV2	=	0x36	; Reserved
RESV3	=	0x37	; Reserved
RESV4	=	0x38	; Reserved
OPTION	=	0x39	; system configuration Options
COPRST	=	0x3A	; Arm/Reset COP timer circuitry
PPROG	=	0x3B	; EEPROM Programming register
HPRIO	=	0x3C	; Highest Priority Interrupt and misc.
INIT	=	0x3D	; RAM and I/O Mapping Register
TEST1	=	0x3E	; factory Test register
CONFIG	=	0x3F	; Configuration Control Register

; Interrupt Vector locations

SCIINT	=	0xD6	; SCI serial system
SPIINT	=	0xD8	; SPI serial system
PAIINT 	=	0xDA	; Pulse Accumulator Input Edge
PAOVINT =	0xDC	; Pulse Accumulator Overflow
TOINT	=	0xDE	; Timer Overflow
TOC5INT	=	0xE0	; Timer Output Compare 5
TOC4INT	=	0xE2	; Timer Output Compare 4
TOC3INT	=	0xE4	; Timer Output Compare 3
TOC2INT	=	0xE6	; Timer Output Compare 2
TOC1INT	=	0xE8	; Timer Output Compare 1
TIC3INT	=	0xEA	; Timer Input Capture 3
TIC2INT	=	0xEC	; Timer Input Capture 2
TIC1INT	=	0xEE	; Timer Input Capture 1
RTIINT	=	0xF0	; Real Time Interrupt
IRQINT	=	0xF2	; IRQ External Interrupt
XIRQINT	=	0xF4	; XIRQ External Interrupt
SWIINT	=	0xF6	; Software Interrupt
BADOPINT =	0xF8	; Illegal Opcode Trap Interrupt
NOCOPINT =	0xFA	; COP Failure (Reset)
CMEINT	=	0xFC	; COP Clock Monitor Fail (Reset)
RESETINT =	0xFE	; RESET Interrupt
		.list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;$Log: 6811regs.asm,v $
;Revision 1.1.1.1  2003/09/15 12:17:40  mikee
;
;
;Revision 1.2  2003/08/29 19:23:27  mikee
;Error corrected.  Ooops!
;
;Revision 1.1.1.1  2003/08/27 00:42:15  mikee
;
;
;Revision 1.4  2003/03/28 12:11:20  mikee
;MOTORS removed as this is specific to HB, not 68HC11
;
;Revision 1.3  2003/03/10 00:57:52  mikee
;Added directives .nolist - .list  Removed AS11 directives
;
;Revision 1.2  2003/02/18 00:34:01  mikee
;Resolve
;
;Revision 1.1  2003/02/05 01:17:28  mikee
;Initial checkin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

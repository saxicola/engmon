; 6811 assembly code to interface with the Hitachi HD44780
; LCD Screen Controller.
; $Id: LCD2.asm,v 1.1.1.1 2003/09/15 12:17:41 mikee Exp $ 
; This code contains all the necessary subroutines to write 
; to the screen. 
; It also includes a simple main program that will execute 
; the instructions. 
; The subroutines are designed to be transferred to other 
; programs and simply dropped in. 
; 
; Important Note: This program r=ires a significant amount 
; of space on the stack. Be sure to initialize the stack 
; before beginning to run these routines. 
; Equates 
; buffalo operations 
	outa 	= 0xffb8 ;output the ASCII character in A 
	outstrg = 0xffca ;output string at x 
	outcrlf = 0xffc4 ;output crlf 
	outlhlf = 0xffb2 ;output left nibble of a in ASCII 
	outrhlf = 0xffb5 ;output right nibble of a in ASCII 
	out2bsp = 0xffc1 ;output 2byte value at x in HEX 
	input = 0xffac ;a=input() ; a=0 if no char entered 
	inchar = 0xffcd ;a=input() ; loop till user enters char 
	upcase = 0xffa0 ;a=upcase(a) 
	wchek = 0xFFA3 ;z=1 if A={space,comma,tab} 
	dchek = 0xFFA6 ;z=1 if A={space,comma,tab,CR} 
; Port Declarations 
	porta = 0x1000 
	portc = 0x1003 
	ddrc =  0x1007 
; This is the main program that calls the subroutines. 
	.section	ram,"ax"
	jmp start 
temp: 	rmb 1 
; data to be displayed on the screen 
test1: 	fcc "point a" 
	fcb 0x04 
test2: 	fcc "point b" 
	fcb 0x04 
start:
	lds #0xDFFF 
; initialize the stack.
	ldaa #0x3F 
; Load the screen type 
	jsr initlcd 
; initialize the screen 
	ldaa #0x80 
; set address to 0 
	ldab #0x00 
; set control pins 
	jsr writelcd 
	ldaa #0x47 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x6F 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x6F 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x64 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x20 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x4d 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x6f 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x72 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x6e 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x69 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x6e 
; write a character
	ldab #0x10 
	jsr writelcd 
	ldaa #0x67 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0xc0 
; write address 
	ldab #0x00 
	jsr writelcd 
	ldaa #0x44 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x61 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x76 
; write a character 
	ldab #0x10 
	jsr writelcd 
	ldaa #0x65 
; write a character 
	ldab #0x10 
	jsr writelcd 
	swi 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ; 
;Initlcd ; ; This subroutine initializes the LCD screen. The LCD 
; screen format is passed in by the A register and is 
; stored on the stack. 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
initlcd: 
	psha 
; save the lcdtype 
	ldaa #0x00 
; clear ports A and C 
	staa portc 
	staa porta 
	staa ddrc 
	ldx #0x9c40 
; wait for ~15ms 
loop: 
	dex 
	cpx #0x0000 
	bne loop 
	ldaa #0xff 
; set port C for output 
	staa ddrc 
	ldaa #0x3f 
; write the function set command 
	staa portc
	jsr delay 
; delay function 
	ldaa #0x20 
; pulse the enable bit 
	staa porta 
	jsr delay 
	ldaa #0x00 
; turn off enable 
	staa porta 
	staa portc 
	ldx #0x2328 
; wait ~4.1 ms 
loop2: 
	dex 
	cpx #0x00 
	bne loop2 
	ldaa #0x3f 
; write the function set command 
	staa portc 
	jsr delay 
	ldaa #0x20 
; pulse the enable bit 
	staa porta 
	jsr delay 
	ldaa #0x00 
; turn off enable 
	staa porta 
	staa portc 
	ldx #0x1f4 
; wait ~100 us 
loop3: 
	dex 
	cpx #0x00 
	bne loop3 
	ldaa #0x3f 
; write the function set command 
	staa portc 
	jsr delay 
	ldaa #0x20 
; pulse the enable bit 
	staa porta 
	jsr delay 
	ldaa #0x00 
; turn off enable 
	staa porta 
	pula 
; get the lcd type from the stack 
	ldab #0x00 
	jsr writelcd 
; write the # of lines and font 
	ldaa #0x0C 
; clear screen 
	ldab #0x00 
	jsr writelcd 
; write it to the screen 
	ldaa #0x01 
; set cursor to address 0.
	ldab #0x00 
	jsr writelcd 
	rts 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
; Writelcd ; 
; This function checks the busy flag and then writes 
; either data or instructions to the LCD screen. 
; The data to be written is stored in register A 
; and the control pin settings are stored in register 
; B. These are stored on the stack until they are 
; needed. 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
writelcd: 
	pshb 
; store the rs and rw values 
	psha 
; store the data 
	ldaa #0x00 
	staa ddrc 
; set port C for input 
loop4: 
	ldaa #0x08 
; checking the busy flag 
	staa porta 
	jsr delay 
	ldaa #0x28 
; pulse the enable 
	staa porta 
	jsr delay 
	ldaa portc 
; read port C 
	anda #0x80 
; check the busy flag 
	cmpa #0x80 
; if flag set, loop else 
	beq loop4 
	ldaa #0x00 
; clear the enable 
	staa porta 
	ldaa #0xff 
; set port C for output 
	staa ddrc 
	pula 
; write data to port C 
	staa portc 
	pulb 
; write control pins to port A 
	stab porta 
	stab temp 
	jsr delay 
	ldaa temp 
	oraa #0x20 
; set the enable and write it to port A
	staa porta 
	jsr delay 
	ldaa #0x00 
; clear control pins 
	staa porta 
	rts 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
; delay ; ; This function creates a delay to allow pins time to set up 
; and stabilize.
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
delay: 
	ldab #0x1E 
; wait 18 counts 
waitloop: 
	decb 
	cmpb #0x00 
	bne waitloop 
	rts

;$Log: LCD2.asm,v $
;Revision 1.1.1.1  2003/09/15 12:17:41  mikee
;
;


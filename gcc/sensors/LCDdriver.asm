;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;This is my not-so-sad attempt at an LCD driver
;$Id: LCDdriver.asm,v 1.1.1.1 2003/09/15 12:17:40 mikee Exp $
; This HAS to run in CHIP RAM as it turns OFF EXTERNAL memory whilst executing
; so external memory is not acessable.  THis means that any text you need to write
; first needs to be copied to CHIP RAM and the address and length written to a some
; pointer address.  Aaargghhhh!!!!!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	.include	"defines.asm"	; The registers



	.section	ram,"ax"
EnableLCD:  ldx #0x1000

        sei             ; disable interrupts
        
        ;ldaa   #0xc5
        ;staa   HPRIO,x   ; put into single chip mode or ...
        bclr    HPRIO,x, #0b00100000   ; Clears the MDA bit, Mode Select A
        
        bset    DDRA,x,  #0b11110000    ; Set port A bit 4  to output
        bclr    PORTA,x, #0b00010000    ; turn off LCD E line
        ;jsr    0xfea5          ;Beep
        
        ldaa    #0xFF
        staa    DDRC,x          ; make port C output
        
        
        
        ldaa    #0x00           ; In command mode
        ldab    #0x30           ; Power on LCD
        staa    PORTB,x         ; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     wait240us
        stab    PORTC,x         ; low byte is data
        jsr     wait240us
        bclr    PORTA,x, #0b00010000
        
        jsr     wait10ms
        
        ldaa    #0x00           ; In command mode
        ldab    #0x34                   ; Sets LCD to two lines, 8bit
        staa    PORTB,x         ; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     wait240us
        stab    PORTC,x         ; low byte is data
        jsr     wait240us
        bclr    PORTA,x, #0b00010000
        jsr     wait10ms
        
        ldaa    #0x00           ; In command mode
        ldab    #0x34                   ; Sets LCD to two lines, 8bit
        staa    PORTB,x         ; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     wait240us
        stab    PORTC,x         ; low byte is data
        jsr     wait240us
        bclr    PORTA,x, #0b00010000
        jsr     wait10ms
        
        
        ldaa    #0x00
        ldab    #0x0e                   ;Switch on display
        staa    PORTB,x         ; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     wait240us
        stab    PORTC,x         ; low byte is data
        jsr     wait240us
        bclr    PORTA,x, #0b00010000
        jsr     wait10ms
        
        
        ldaa    #0x00
       
        ldab    #0x06                   ;Set LCD to inc & shift on write
        staa    PORTB,x         ; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     wait240us
        stab    PORTC,x         ; low byte is data
        jsr     wait240us
        bclr    PORTA,x, #0b00010000
        jsr     wait10ms
        
        
        ldaa    #0x00
        
       	ldab    #0x01                   ; Goto start"
       	staa    PORTB,x         ; high byte is control
       	bset    PORTA,x, #0b00010000
       	jsr  	wait240us
       	stab    PORTC,x         ; low byte is data
       	jsr 	wait240us
       	bclr    	PORTA,x, #0b00010000
       	ldab    	#0x00          ;Prep to set data = 0
       	jsr 		wait240us
       	stab    	PORTC,x            ;Set data = 0
       	jsr    	wait10ms


        
        jsr     WriteStuff
      

 Reset: 
 		bset    PORTA,x, #0b00000000
        bclr    DDRA,x,  #0b00001000    ; Set port A bit 4  to input
        ;ldaa   #0xe5
        ;staa   HPRIO,x    ; put back into expanded chip mode
        bset    HPRIO,x 	#0b00100000 ; Set the MDA bit, Mode Select A
        cli 	;enable interrupts
        swi		;programmed interrupt or...
        rts     ; return to monitor command loop
        

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
wait1ms:
		ldy #286    ;1ms
        bra wm10
wait720us:
		ldy #100    ;720us
  		bra wm10
wait240us:
		ldy #30     ;240us
  		bra wm10
wait90us:      
		ldy #10     ;90us

wait10ms:
    	ldy #2857           ;* 285*7=19999 clocks ~= 10ms
wm10:
    	dey             ;* 4
    	bne wm10        ;* 3
    	rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Write some data to the screen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
WriteStuff:     
		pshx
        ldx #Hello  ; Point to string this could be an address the holds a pointer to the string
	    ; this is  a more useful scenario so we can reuse this routine from a program in external RAM/ROM
	    ; define this an all up or summat like...
		;ldx 0x64	; loads the address stored in 0x64 which points to the text

NextChar:       
        ldab    0,x
        beq     StartAgain  ; End of string = '\0'
        jsr     LCDwrite    ;Write it to the LCD
        inx
        jsr 	wait240us
        bra 	NextChar

StartAgain:
		pulx
        rts
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Write to the LCD Data to be writ is in ACC B
LCDwrite:       
            pshx                    ;save it
            psha
            ldx     #0x1000                 ;The regs base
            ldaa    #0x02
            staa    PORTB,x         ; high byte is control
            jsr 	wait240us
            bset    PORTA,x, #0b00010000    ;Set E =HI
            jsr     wait240us
            stab    PORTC,x         ; Send data to LCD
            ldab    #0x00           ;Prep to set data = 0
            jsr     wait240us       ;Wait for it to stabilise
            bclr    PORTA,x, #0b00010000    ;Set E = LO so latching data into lcd
            jsr     wait240us
            stab    PORTC,x         ;Set data = 0
            ;jsr    wait240us
            pula
            pulx
            rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Put a character to SCI from B register, clobbers A
OUTSCI:
        ldaa    REGS+SCSR   ; Wait for TDRE
        anda    #TDRE       ; output ready?
        beq 	OUTSCI      ; not yet
        stab    REGS+SCDR   ; send it, *SETTING CONDITION CODES*
        rts
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Timer
Wait:
        ldd     0x100E                  ;Get current timer value
        addd    #5                      ;Nms worth of ticks
        std     0x101a                  ;Store the new value in the PORTA3
        bclr    0x23,x, #0x20           ;Clear TFLG1 OC3F
Chkflg:         brclr   0x23,x, #0x20, Chkflg   ;Loop 'till set
        rts                             ;Return
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Poll for input character:
INPOLL:
		ldab    REGS+SCSR   ; Wait for RDRF
        andb    #RDRF       ; character available?
        beq 	INPOLL_EXIT ; not yet
        ldab    REGS+SCDR   ; get it
        cmpb    #3      ; ^C?
        bne 	STARTAGAIN  ; No, start output string again
        swi         ; Programmed breakpoint
        bra 	INPOLL_EXIT
STARTAGAIN:
		;ldx    #HelloWorld ; Point to string
INPOLL_EXIT:
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;The Greeting text
Hello:
		.asciz  "MONKEY V's ROBOT"


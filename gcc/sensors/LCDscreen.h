// LCD Screen routines for the Motorola 6811 using a Hitachi 
// HD44780
// Written by Lee Rosenberg - rosenl@rpi.edu
// Developed for use with Introl C 4.0
// October 21, 1998
#include <HC11A1.h> 
void OpenXLCD(char);
// configures I/O pins for external
LCD void SetDDRamAddr(char);
// sets display data address
char BusyXLCD(void);
// returns busy status of the LCD
void WriteCmdXLCD(char);
// write a command to the LCD
void WriteDataXLCD(char);
// writes data byte to the LCD
void WriteBuffer(char *buffer);
//Writes a string to the LCD
/*********************************************************** Write Buffer Function:
Write a string of bytes to the HD44780 
Input Parameters: char *buffer 
Return Type: None 
***********************************************************/ 
void WriteBuffer(char *buffer) 
{ 
    while(*buffer) // while buffer not empty
    { while(BusyXLCD()); // check if screen busy
        WriteDataXLCD(*buffer); // write a character
        buffer++;
        // increment pointer
    } return;
}
/**********************************************************
OpenXLCD Function: This configures the LCD screen. 
Input Parameters: char lcdtype 
Return Type: None 
Notes: This function must be run before the LCD screen can be used. 
*********************************************************/
void OpenXLCD(char lcdtype)
{
    int i; _H11PORTC = 0;
    // initialize control port A and _H11DDRC = 0x00;
    // Data port C
    _H11PORTA = 0x00;
    // delay for 15ms. This is customized for the HC11 and must
    //be changed for other processors
    for(i=0; i<40,000; i++);
    // set up interface to LCD
    _H11DDRC = 0xFF; _H11PORTC = 0x3F;
    // Function set command (8 bit)
    _H11PORTA = 0x20; // clock command in
    for(i=0; i<30;i++); // delay for ~ 15 us
    _H11PORTA = 0x00; // delay for at least 4.1 ms
    for(i=0;i<9000;i++); // setup interface
    _H11PORTC=0x3F; // Function set command (8 bit)
    _H11PORTA = 0x20; // clock in command
    for(i=0;i<30;i++); // delay for ~15 us
    _H11PORTA = 0x00;
    // delay for at least 100us
    for(i=0;i<500;i++); // set up interface
    _H11PORTC = 0x3F; // function set command (8 bit)
    _H11PORTA = 0x20; for(i=0;i<30;i++); // delay for ~15 us
    _H11PORTA = 0x00; WriteCmdXLCD(lcdtype); // function set 8 bit interface
    WriteCmdXLCD(0x0C);
    WriteCmdXLCD(0x01); // turn off display
    return;
}

/********************************************************
WriteCmdXLCD Function: Writes a command to the controller 
Input Parameter: char cmd
Return Type: None 
Notes: Before writing the command the function checks that the display is not busy by calling BusyXLCD. 
*********************************************************/
void WriteCmdXLCD(char cmd)
{
    int i; while(BusyXLCD()); // Check LCD is not in use
    _H11DDRC = 0xFF; _H11PORTC = cmd; // write cmd to port
    _H11PORTA = 0x00; // set control signals
    for(i=0; i<30; i++); // delay for ~15 us
    _H11PORTA=0x20; // clock in the command
    for(i=0;i<30;i++); // delay for ~15 us
    _H11PORTA=0x00; for(i=0;i<30;i++); // delay for ~15 us
    _H11DDRC=0x00;
    return;
}
/********************************************************
SetDDRamAddr Function: Set the address of the LCD controller 
Input Parameter: char DDaddr 
Return Type: None 
Notes: This function sets the address of the LCD screen to the address that is passed in as a char. 
The address is automatically modified to the correct format for the screen. 
**********************************************************/ 
void SetDDRamAddr(char DDaddr)
{
    int i;
    while(BusyXLCD()); // check if screen is in use
    _H11DDRC=0xFF; _H11PORTC=(DDaddr | 0x80);// write cmd and addr to port
    _H11PORTA=0x00; for(i=0;i<30;i++); // delay for ~15 us
    _H11PORTA=0x20; // clock in the command
    for(i=0;i<30;i++); // delay for ~15 us
    _H11PORTA=0x00;
    for(i=0;i<30;i++); // delay for ~15 us
    _H11DDRC =0x00;
    return;
}
/***********************************************************
BusyXLCD Function: This checks the busy status of the HD 44780 
Input Parameter: None 
Return Type: char 
Notes: This is necessary to ensure that the LCD screen is ready to recieve data. 
**********************************************************/ 
char BusyXLCD(void)
{
    int i;
    _H11DDRC=0x00;
    _H11PORTA=0x08; // set control bits
    for(i=0;i<30;i++); // delay for ~15 us
    _H11PORTA=0x28; // clock them in
    for(i=0;i<30;i++); // delay for ~15 us
    if(_H11PORTC & 0x80) // read busy flag
    {
        _H11PORTA = 0x00; // if set return 1;
    } 
    else
    {
        _H11PORTA = 0x00; //
        if clear return 0;
    }
}
/*********************************************************
WriteDataXLCD Function: Writes data to the LCD 
Input Parameter: char data 
Return Type: None 
Notes: This function takes ascii data and writes it to the LCD screen. 
All data is passed in as a char. 
**********************************************************/ 
void WriteDataXLCD(char data)
{
    int i;
    while(BusyXLCD()); // check if screen is ready
    _H11DDRC = 0xFF;
    _H11PORTC = data; // write data
    _H11PORTA = 0x10;
    for(i=0;i<30;i++); // delay for ~15 us
    _H11PORTA=0x30; // clock in data
    for(i=0;i<30;i++); // dlay for ~ 15 us
    _H11PORTA=0x00;
    _H11DDRC= 0x00; return;
}



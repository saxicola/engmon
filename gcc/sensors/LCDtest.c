/* Basic test program for the Hitachi HD 44780
This will test all the major functions and commands to ensure that the screen is functioning correctly. 
*/ 
// All necessary include statements
#include <HC11A1.h> 
// register declarations 
#include<introl.h> // Introl functions 
#include<stdio.h> // I/O commands 
#include<stdlib.h> // Standard C functions 
#include<lcd11.h> // LCD functions

void main()
{
    int i, j;
    char buffer[]="hello";
    OpenXLCD(0x3F); //intialize the screen
    WriteCmdXLCD(0x80); // set address to 0
    WriteDataXLCD(0x47); // write "Good Morning Dave"
    WriteDataXLCD(0x6F);
    WriteDataXLCD(0x6F);
    WriteDataXLCD(0x64);
    WriteDataXLCD(0x20);
    WriteDataXLCD(0x4D);
    WriteDataXLCD(0x6F);
    WriteDataXLCD(0x72);
    WriteDataXLCD(0x6E);
    WriteDataXLCD(0x69);
    WriteDataXLCD(0x6E);
    WriteDataXLCD(0x67);
    WriteCmdXLCD(0xC0);
    WriteDataXLCD(0x44);
    WriteDataXLCD(0x61);
    WriteDataXLCD(0x76);
    WriteDataXLCD(0x65);
    WriteCmdXLCD(0x08); // turn off display
    for(i=0; i<10; i++) // delay
        for(j=0; j<40000; j++);
    WriteCmdXLCD(0x0C); // turn on display and cursor
    for(i=0; i<10; i++) // delay
        for(j=0; j<40000; j++);
    SetDDRamAddr(0xCA); // set cursor address to 4F
    WriteBuffer(&buffer); //write buffer to screen
    for(i=0;i<10;i++) // delay
        for(j=0;j<40000;j++);
    SetDDRamAddr(0x90); // go to address 16
    WriteDataXLCD(0x41); // write ABCDE
    WriteDataXLCD(0x42);
    WriteDataXLCD(0x43);
    WriteDataXLCD(0x44);
    WriteDataXLCD(0x45);
    WriteCmdXLCD(0x18); // shift display left 5 times
    WriteCmdXLCD(0x18); 
    WriteCmdXLCD(0x18);
    WriteCmdXLCD(0x18); 
    WriteCmdXLCD(0x18);
    for(i=0;i<10;i++) // delay
        for(j=0;j<40000;j++);
    WriteCmdXLCD(0x01); // clear display
}

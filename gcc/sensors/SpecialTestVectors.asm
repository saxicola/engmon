; A vector table for when the HB is used in special test mode.
;.file SpecialTestVectors.asm
;$Id: SpecialTestVectors.asm,v 1.1.1.1 2003/09/15 12:17:40 mikee Exp $

;------------------------------------------------------------------------------------ 

;------------------------------------------------------------------------------------
		.section	vectors,"ax"
		
		.=+0x16			; Addresses below this are reserved
SCISS_V:	.byte	0x80,0x02	; SPI Serial System
SPISTC_V:	.byte	0x00,0xC7	; SPI Serial Transfer Complete
PAIE_V:		.byte	0x00,0xCA	; Pulse Accumulator Input Edge
PAO_V:		.byte	0x00,0xCD	; Pulse Accumulator Overflow
TO_V:		.byte	0x00,0xD0	; Timer Overflow
TIC4OC5_V:	.byte	0x00,0xD3	; Timer Input Capture 4/ Output Compare 5
TOC4_V:		.byte	0x00,0xD6	; Timer Output Compare 4
TOC3_V:		.byte	0x00,0xD9	; Timer Output Compare 3
TOC2_V:		.byte	0x00,0xDC	; Timer Output Compare 2
TOC1_V:		.byte	0x00,0xDF	; Timer Output Compare 1
TIC3_V:		.byte	0x00,0xE2	; Timer Input Capture 3
TIC2_V:		.byte	0x00,0xE5	; Timer Input Capture 2
TIC1_V:		.byte	0x00,0xE8	; Timer Input Capture 1
RTI_V:		.byte	0x00,0xEB	; Real Time Interrupt
IRQ_V:		.byte	0x00,0xEE	; /IRQ
XIRQ_V:		.byte	0x00,0xF1	; /XIRQ pin
SI_V:		.byte	0x00,0xF4	; Software Interrupt
IOT_V:		.byte	0x80,0x00	; Illegal opcode trap
COP_V:		.byte	0x80,0x00	; COP Failure
CMF_V:		.byte	0x00,0xFD	; Clock Monitor Fail
RESET_V:	.byte	0x80,0x00	; Reset


;------------------------------------------------------------------------------------
; $Log: SpecialTestVectors.asm,v $
; Revision 1.1.1.1  2003/09/15 12:17:40  mikee
;
;
; Revision 1.2  2003/08/29 20:35:18  mikee
; Oh dear, addresses are 16bit, 2 bytes!
;
; Revision 1.1  2003/08/29 19:59:26  mikee
; Initial Check In
;

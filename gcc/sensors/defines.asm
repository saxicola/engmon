/*
*
* $Author: mikee $
* $Revision: 1.1.1.1 $
* $Log: defines.asm,v $
* Revision 1.1.1.1  2003/09/15 12:17:40  mikee
*
*
* Revision 1.2  2001-10-04 18:19:19+01  mfe99
* New definitions added
*
* Revision 1.1  2001-10-04 18:06:19+01  mfe99
* Initial revision
*
*/

    
PORTA           =       0x00            ;PORTA
DDRA            =       0x01
PORTB           =       0x04            ;PORTB Upper 8 bits of external address bus
PORTF           =       0x05            ;PORTF Lower 8 bits of external address bus
PORTC           =       0x06            ;PORTC
DDRC            =       0x07        ;Port c Data Direction
TCNT            =       0x0e
PORTA1          =       0x16
PORTA2          =       0x18
PORTA3          =       0x1a
PORTA4          =       0x1d
TI4O5           =       0x1e
TCTL1           =       0x20
TFLG1           =       0x23
TMSK2           =       0x24
PACL            =       0x26
HPRIO           =       0x3c
REGS            =       0x1000          ;Device register base
SCCR2           =       0x2D    ; SCI control register 2
SCSR            =       0x2E    ; SCI status register
SCDR            =       0x2F    ; SCI data register
RDRF            =       0x20    ; SCSR "Received Data Ready" flag
CSCTL       =   0x5d    ; CSCTL configures chip-select
CSGADR      =   0x5e    ;CSGADR configures base address of General Purpose chip select;
CSGSIZ      =   0x5f    ;CSGSIZ configures General Purpose chip select chip size
TDRE            =       0x80    ; SCSR "Transmit Data Register Empty" flag
MOTORS      =   0x7000  ; Where the motors are in single chip mode

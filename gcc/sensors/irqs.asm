; File: irgs.asm
;$Id: irqs.asm,v 1.1.1.1 2003/09/15 12:17:40 mikee Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Set up any interrupt vectors in the lower memory beginning at 0x00C7.  
; See memory.x for location
; Change from defaults as needed by application
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		.section 	irqs,"ax"
		jmp	0x8000		; SPI Serial Transfer Complete
		jmp	0x8000		; Pulse Accumulator Input Edge
		jmp	0x8000		; Pulse Accumulator Overflow
		jmp	0x8000		; Timer Overflow
		jmp	0x8000		; Timer Input Capture 4/ Output Compare 5
		jmp	0x8000		; Timer Output Compare 4
		jmp	0x8000		; Timer Output Compare 3
		jmp	0x8000		; Timer Output Compare 2
		jmp	0x8000		; Timer Output Compare 1
		jmp	0x8000		; Timer Input Capture 3
		jmp	0x8000		; Timer Input Capture 2
		jmp	0x8000		; Timer Input Capture 1
		jmp	T_SIRQ		; Real Time Interrupt
		jmp	0x8000		; /IRQ
		jmp	0x8000		; /XIRQ pin
		jmp	0x8000		; Software Interrupt
		jmp	0xfe00		; Illegal opcode trap
		jmp	0x8000		; COP Failure
		jmp	0x8000		; Clock Monitor Fail
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;$Log: irqs.asm,v $
;Revision 1.1.1.1  2003/09/15 12:17:40  mikee
;
;
;Revision 1.1.1.1  2003/08/27 00:42:15  mikee
;
;
;Revision 1.1  2003/03/28 20:49:08  mikee
;Initial checkin with mostly default values
;

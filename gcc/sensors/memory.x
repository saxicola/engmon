MEMORY
  {
    ramex : org	= 0x8000, l = 32K
    ramex-hi : org = 0xc000, l = 0x3e00
    eeprom   : ORIGIN = 0x8000, LENGTH = 449
    ram  : org = 0x100, l = 768
    irqs : org = 0x00c7, l = 60
    vectors : org = 0xBFC0, l = 64
  }


SECTIONS {
ram : { *(.ram) } > ram
eeprom : { *(.eeprom) } > eeprom
ramex : { *(.ramex) } > ramex
irqs : { * (.irqs) } > irqs
vectors : { * (.vectors) } > vectors
}
 

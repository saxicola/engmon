; File: sensor.asm
; Sensor controller code for 68HC11
; To monitor the analog inputs and warn if any of the voltages go out of range
; To be used as a condition monitor generally or as a yacht engine, gas monitor
; alarm.  Will incorporate LCD code so likely to need additional memory in 
; eeprom
; or initally battery backed up RAM.  We can simplify the LCD drive I suspect.
; Will use the on board watchdog facilties to reset if hung.  Low voltage 
; restart
; chips incorporated on the PCB 

; NB LCD routines
; This HAS to run in CHIP RAM as it turns OFF EXTERNAL memory whilst executing
; so external memory is not acessable.  THis means that any text you need to 
; write first needs to be copied to CHIP RAM and the address and length written 
; to a  some pointer address.  Aaargghhhh!!!!!
; First copy the message to Message area in EEPROM then do LCD routine

; $Id: sensor.asm,v 1.1.1.1 2003/09/15 12:17:40 mikee Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                .title	sensor.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Include 68HC11 register definition file
		.include	"6811regs.asm"


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Take care here as this may clobber the talker, but a proportion of  the talker
; code here only runs once to init some regs etc.

		.section	ram,"ax"


Main:
; Set up a jump table in local RAM so we know where to point the jump vectors
		jmp	Start		; Jump to the code start
RAMBASE		=.			; The current pointer location. Doesn't really point to RAMBASE
inChar:		.byte	0x01		; Whatever the SCI received
DIO:		.byte	0x01		; The digital input status on handyboard only
spTemp:		.byte	0x02		; temp SP value
ana1:		.byte	0x01		; Analog results
ana2:		.byte	0x01		; so that old results may compared to new
ana3:		.byte	0x01		; results
ana4:		.byte	0x01		; Analog results
Hex:		.asciz	"0123456789ABCDEF" ; Used for hex2ASCII conversion
Message:	.asciz 	"0123456789ABCDEF0123456789ABCDEF" 	; 32 char Temporary test
								; area in EEPROM
ASCII:		.ascii	"01"		; A 2 letter ascii number
DBUFR:      	.ascii  "01234" 	; 5 byte decimal ASCII number.  NOT an output 
					; string holder
DecWhole:	.ascii	"01234"		; Five bytes for whole decimal parts
Point:		.ascii	"."		; The decimal point
DecFract:	.ascii	"01"		; Two byte fraction part
CrLf:		.asciz	"\n\r"		; CrLr and zero string terminator
MATH1:		.byte	0,0		; Two fixed point numbers needing to be
                                	; multiplied together; 0 to 255:255
MATH2:		.byte	0,0		; Result ends up in MATHRESULT
					; also used for addition temp regs ;-)
MATHRESULT:	.byte	0,0,0,0		; The result of the multiply.  Four bytes
					; with decimal part in bytes 3 & 4.
					; byte 4 mostly ignored as innacurate
Thermo:		.byte	0,0,0,0		; four bytes for the temperature in decimal hex

MOTORS		=	0x7000		; The motor controller address
;------------------------------------------------------------------------------------
; This section MUST be in internal RAM along with the text to be writ

EnableLCD:  ldx #0x1000

        sei             		; disable interrupts
        
        ;ldaa   #0xc5
        ;staa   HPRIO,x   		; put into single chip mode or ...
        bclr    HPRIO,x, #0b00100000   	; Clears the MDA bit, Mode Select A
        
        bset    DDRA,x,  #0b11110000    ; Set port A bit 4  to output
        bclr    PORTA,x, #0b00010000    ; turn off LCD E line
        ;jsr    0xfea5          	;Beep
        
        ldaa    #0xFF
        staa    DDRC,x          	; make port C output
        
        
        
        ldaa    #0x00           	; In command mode
        ldab    #0x30           	; Power on LCD
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     dly240
        stab    PORTC,x         	; low byte is data
        jsr     dly240
        bclr    PORTA,x, #0b00010000
        
        jsr    	dly10
        
        ldaa    #0x00           	; In command mode
        ldab    #0x34                   ; Sets LCD to two lines, 8bit
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     dly240
        stab    PORTC,x         	; low byte is data
        jsr     dly240
        bclr    PORTA,x, #0b00010000
        jsr     dly10
        
        ldaa    #0x00           	; In command mode
        ldab    #0x34                   ; Sets LCD to two lines, 8bit
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     dly240
        stab    PORTC,x         	; low byte is data
        jsr     dly240
        bclr    PORTA,x, #0b00010000
        jsr     dly10
        
        
        ldaa    #0x00
        ldab    #0x0e                   ;Switch on display
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr    dly240
        stab    PORTC,x         	; low byte is data
        jsr     dly240
        bclr    PORTA,x, #0b00010000
        jsr     dly10
        
        
        ldaa    #0x00
       
        ldab    #0x06                   ;Set LCD to inc & shift on write
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     dly240
        stab    PORTC,x         	; low byte is data
        jsr     dly240
        bclr    PORTA,x, #0b00010000
        jsr     dly10
        
        
        ldaa    #0x00
        
       	ldab    #0x01                   ; Goto start"
       	staa    PORTB,x         	; high byte is control
       	bset    PORTA,x, #0b00010000
       	jsr  	dly240
       	stab    PORTC,x         	; low byte is data
       	jsr 	dly240
       	bclr   	PORTA,x, #0b00010000
       	ldab   	#0x00          		;Prep to set data = 0
       	jsr 	dly240
       	stab   	PORTC,x            	;Set data = 0
       	jsr    	dly10
		rts			; Return 

      
;----------------------------------------------------------------------------
 ResetLCD: 
 	bset    PORTA,x, #0b00000000
        bclr    DDRA,x,  #0b00001000    ; Set port A bit 4  to input
        ;ldaa   #0xe5
        ;staa   HPRIO,x    		; put back into expanded chip mode
        bset    HPRIO,x #0b00100000 	; Set the MDA bit, Mode Select A
        cli 				;enable interrupts
        swi				;programmed interrupt or...
        rts     			; return to monitor command loop
        
;--------------------------------------------------------------------------------
;Write to the LCD Data to be writ is in ACC B
LCDwrite:       
        pshx                    	;save it
	psha
	ldx     #0x1000         	;The regs base
	ldaa    #0x02
	staa    PORTB,x         	; high byte is control
	jsr     dly240
	bset    PORTA,x, #0b00010000    ;Set E =HI
	jsr     dly240
	stab    PORTC,x         	; Send data to LCD
	ldab    #0x00           	;Prep to set data = 0
	jsr     dly240       		;Wait for it to stabilise
	bclr    PORTA,x, #0b00010000    ;Set E = LO so latching data into 
						; lcd
	jsr     dly240
	stab    PORTC,x         	;Set data = 0
	;jsr    dly240
        pula
        pulx
        rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                .section   ramex,"ax"     ; program area external RAM, variable 
; storage 32K


		bra 	Start		; reset vector points here, = 0xfe00, or will in EEPROM
					; jmp = 3 bytes.  bra=2 bytes in same page only
; Preset message list.  To be copied to internal RAM for display must be < 32 chars
Message0:	.asciz	""
Message1:	.asciz	"HIGH EXHAUST TEMP"
Message2:	.asciz	"LOW OIL PRESSURE"
Message3:	.asciz	"LOW BATTERIES"
Message4:	.asciz	"HIGH WATER TEMPERTURE"
Message5:	.asciz	"GAS IN BILGES"
Message6:	.asciz	"OTHER"
Message7:	.asciz	""
Message8:	.asciz	""
Message9:	.asciz	""

Start:
	lds	#0x03ff		;Set new stack pointer to internal RAM top
	;bset	OPTION,x,#0x03	; Set the COP timpout to 1.049s, i.e. longest possible
	;bclr 	CONFIG,x,#0x04	; Set the NOCOP = 0, turn COP system ON
	ldaa	#0xff		; for
	staa	MOTORS		; testing
	ldx	#REGS		; set up asyc I/O on SCI
	bset 	SPCR,x,#0x20
	bset	SCCR2,x,#0x0c
	ldd	#0x300c
	staa	BAUD,x
	stab	SCCR2,x
	ldaa	#0
	staa	SCCR1,x


Regreet:
	jsr	SendGreet
	ldaa	#0x00
	staa	MOTORS		; Switch motors off
	;jsr	EnableCSI
; Zero our storage.  Since A=0 we can do...
	staa	inChar


	bra 	Monitor		; Go to the monitor loop
STOP:
	;lds	spTemp		;restore the SP
	stop			;STOP
	jmp Start		;Do it again. (and prevents continue into code)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Monitor loop for input/output monitoring after the initial set up
; Monitors RS232, A/D, digital inputs and switches on HB
Monitor:
	jsr	TestIO		; Get the HB IO status to DIO
	ldaa	#0xbf		; Test if STOP button pressed
	cmpa	DIO
	beq	STOP
	ldaa	#0x7f
	cmpa	DIO		; Test for START button pressed
	beq	Regreet
	jsr	INSCI
	jsr	DoInstruction	; Check the pressed key and execute whatever
	;ldaa	#0x55		; reset the COP watchdog, if enabled
	;staa	COPRST+REGS
	;ldaa 	#0xAA
	;staa	COPRST+REGS
	bra	Monitor


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Send a logon greeting message
Greeting:		.asciz	"$Revision: 1.1.1.1 $\n\n\r"
SendGreet:
	ldx	#Greeting	; Point to greeting string
SendChar:
	ldab	0,x
	beq	SendGreetEND	; End of string if 0, so return
	stab	MOTORS		; Just for testing, flashes LEDS
	inx
	jsr	dly20
	jsr	dly20
	jsr	OUTSCI		;
	;jsr	INPOLL
	bra	SendChar
SendGreetEND:	
	rts			;return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Put a character to SCI from B register, clobbers A
OUTSCI:
	pshx
	ldx	#REGS
	psha

LOOPB:
	ldaa	REGS+SCSR	; Wait for TDRE
	anda	#TDRE		; output ready?
	beq	LOOPB		; not yet
	stab	REGS+SCDR	; send it,
	pula
	pulx
	rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Poll for input character.  Char ends up in acc B
INPOLL:
	pshx
	ldx	#REGS
	ldab	REGS+SCSR	; Wait for RDRF
	andb	#RDRF		; character available?
	beq	INPOLL_EXIT	; not yet
	ldab	REGS+SCDR	; get it
	cmpb	#3		; ^C?
	bne	INPOLL		; No, loop again
	swi			; Programmed breakpoint on ^C
	bra	INPOLL_EXIT

INPOLL_EXIT:	
	stab	inChar
	pulx
	rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get character from SCI into B register
INSCI:
		ldx	#REGS
		ldab    REGS+SCSR       ; Wait for RDRF
                andb    #RDRF           ; character available?
                beq     INSCI_END       ; not yet
                ldab    REGS+SCDR       ; get it
		jsr 	OUTSCI		; Echo it first
		stab	inChar
INSCI_END:      rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Motor control routine.  Controlled by the values in Acc B
; Lower nibble contains motor to be controlled, upper nibble contains speed 
; setting
; Lower nibble bits are
; | M1 ON/OFF | M1 F/R | M2 ON/OFF | M2 F/R |
; Motor speed is pulse width modulated using TOC1 to control the ON/OFF cycle
; Full power = no off cycle
; Stop = no on cycle
; A nibble gives us 15 theoretical speed settings
Motors:

		rts

; Handle the TOC set/reset etc.
TOC:
		rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get a value from the A/D input
; Input method/port is defined by the contents of Acc B
; The method of A/D is defined by the contents of ADCTL in the following way
; | CCF | 0 | SCAN | MULT | CD | CC | CB | CA |
; CCF = Conversion complete flag
; SCAN, 1 = Convert group continuously, 0 = one convert then stop
; MULT, 1 = Convert four channels in group, 0 = convert single channel
; The lower nibble controls the Channel selection if MULT bit = 1
; CD:CC:CB:CA | Channel | Result in |
;    0000     |   AN0   |   ADR1    |
;    0001     |   AN1   |   ADR2    |
;    0010     |   AN2   |   ADR3    |
;    0011     |   AN3   |   ADR4    |
;    0100     |   AN4   |   ADR1    |
;    0101     |   AN5   |   ADR2    |
;    0110     |   AN6   |   ADR3    |
;    0111     |   AN7   |   ADR4    |
;
; Other combinations are for system testing
; Bits CD:CC select a conversion group if MULT = 1,
; otherwise if mult = 0 a single channel is converted four times and the result
; is placed in ADR1-4OPTION,x,#0b10000000	;Switch the A/D on
; Bit 0 of result is 0.39% of FSD, or 0.0195V where Vrh = 5V
;
;
;
GetAD:
		ldx	#REGS
		bset	OPTION,x,#0b10000000	;Switch the A/D on
		;jsr	dly			; Allow system to stabilise
		stab	ADCTL,x			; Select channels and start conversion
		brclr	ADCTL,x,#0x80,.		; Is the CCF bit set?
		ldd	ADR1,x			; Store the data
		ldd	ADR3,x			; 2 bytes a go uses less code
		bclr	OPTION,x,#0b10000000	; Switch the A/D off
		rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Do the instruction
DoInstruction:
		ldaa	inChar		; load inst, value to accA accB
		cmpa	#0x74		; Is it an t ?
		beq	DoTemp
		;cmpa	#0x64		; Is is a d ?
		;beq	DoDig
		;cmpa	#0x66		; Is it an f ?
		;beq	StoreF
		;cmpa	#0x78		; Is is an x ?
		;beq	Quit

		; Clear inst before return
		clr	inChar
		rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Test for the switch status and set DIO byte as appropriate
; This only works on the handyboard but could be extended to NLDS with
; Appropriate decoding logic
; START button is bit D7
; STOP  button is bit D6
; So could decode and set a byte here but more efficent to save as a bit pattern
; as this saves at least one byte.
TestIO:
		psha			; Save Acc A
		ldaa	0x7000
		staa	DIO
		pula			; restore Acc A
		rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Return the temperature via RS232
; A small conversion table below:
;A/D	C
;81	31.5
;82	31
;104	24.9
;105	24.4
;105	24
;110	22.5
;114	19.9
;120	18
;116	19.3


DoTemp:
		ldab	#0x14		; Set the A/D mode to get channels 4-7
		jsr	GetAD
		ldab	ADR1+REGS	; Get the data in analog 1
		ldaa 	#0		;
		ldx	#0x3
		idiv			; D/3 -> X
		xgdx			; X -> D.  X contains remainder
		addd	#0xc6		; #0xc6 = -58
		negb			; Do the twos compliment
		ldaa	#0x0		; Clear the hi byte to get rid of the negation bit as not needed
		std	Thermo		; Store the whole degrees part
		xgdx			; Now we do the fractional part.  Remainder to D
		cmpd	#0x2
		bhs	1f		; If greater than 2 then make it 0.5 else
		ldd	#0x3530		; Char 00
		std	DecFract+0
		;ldaa	#0
		;staa	Thermo+2
		bra	2f
1:		ldd	#0x3030		; Char 50
		std	DecFract+0
		;;ldx 	#0x3
		;;fdiv			; Divide by 3 to give parts of a degree in X
		;;xgdx			; X -> D
		;;std	Thermo+2
2:
		ldx	#Thermo		; Pass it to the hextoASCII routine
		jsr	HTOD16		; Convert the data to ASCII Answer in DBUFR
		ldx	#DBUFR		; Get the answer
		ldd	0,x
		std	DecWhole+0	; Store it in output buffer, whole parts
		ldd	2,x
		std	DecWhole+2
		ldaa	4,x
		staa	DecWhole+4
		;ldx	#Thermo+2	; Pass it to the hextoASCII routine
		;jsr	HTOD16		; Convert the data to ASCII
		;ldx	#DBUFR		; Get the answer
		;ldd	0,x
		;std	DecFract+0	; Store it in output buffer, fract parts
		ldx	#DecWhole	; and send it out X = address of String
		jsr	SendChar	; the serial port
		rts			; return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Some delay routines.  Times are in ms
dly10:
		pshx
		ldx	#0x0A
		jsr	dly
		pulx
		rts
dly20:
		pshx
		ldx	#0x14
		jsr	dly
		pulx
		rts
dly100:
		pshx
		ldx	#0x64
		jsr	dly
		pulx
		rts

dly240:
		pshx
		ldx #240     ;240us
  		jsr dly
		pulx
		rts
dly:
		ldy	#0x011C		;timing assumes 8MHz xtal
1:		dey
		bne	1b
		dex
		bne	dly
		rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; HTOD - Subroutine to convert a 16-bit hex number to a
; 5 digit decimal number.
; With addition to convert an 8 bit number
; Uses 5 byte variable "DBUFR" for decimal ASCII result
; On entry X points to hex value to be converted & displayed
; All registers are unchanged upon return
; From Motorolla @ http://www.motorolla.com
; 8 bit routine can jump to the middle of this routine
HTOD8:  	PSHX			;Save registers
		PSHB
		PSHA
		LDD	0,X		;D=hex value to be converted
		bra     1f      	; Jump to the middle as we're only doing 8 bits
; 16 bit routine starts here
HTOD16:		PSHX			;Save registers
		PSHB
		PSHA
		LDD	0,X		;D=hex value to be converted
		LDX	#10000
		IDIV			;D/10,000 -> X; r -> D
		XGDX			;Save r in X; 10,000s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR		;Store in decimal buffer
		XGDX			;r back to D
		LDX	#1000
		IDIV			;D/1,000 -> X; r -> D
		XGDX			;Save r in X; 1,000s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+1		;Store in decimal buffer
		XGDX			;r back to D
1:		LDX	#100		;
		IDIV			;D/100 -> X; r -> D
		XGDX			;Save r in X; 100s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+2		;Store in decimal buffer
		XGDX			;r back to D
		LDX	#10
		IDIV			;D/10 -> X; r in D (B is units digit)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+4		;Store to units digit
		XGDX			;10s digit to D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+3		;Store in decimal buffer
		PULA			;Restore registers
		PULB
		PULX
		RTS			;Return
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;If an illegal opcode is processed(?) then warn the user somehow like a beep
; or something.  This jump needs to be set in EEPROM or in RAM interrupt vector 
; table
IllOpCode:

	jmp	0xFE00			; Jump to program start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This section runs first, on reset and is located in EEPROM
; Set up to jump to start on normal boot
		;.area   CODE2	(ABS)
		;.org	0xfe00
		;.section	eeprom,"ax"
		;jmp	Main


			
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;$Log: sensor.asm,v $
;Revision 1.1.1.1  2003/09/15 12:17:40  mikee
;
;
;Revision 1.2  2003/08/27 00:58:47  mikee
;Inital CI with included header comments
;
;Revision 1.1.1.1  2003/08/27 00:42:15  mikee
;

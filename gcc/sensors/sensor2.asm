; File: sensor2.asm
; Sensor controller code for 68HC11
; To monitor the analog inputs and warn if any of the voltages go out of range
; To be used as a condition monitor generally or as a yacht engine, gas monitor
; alarm.  Will incorporate LCD code so likely to need additional memory in 
; eeprom
; or initally battery backed up RAM.  We can simplify the LCD drive I suspect.
; Will use the on board watchdog facilties to reset if hung.  Low voltage 
; restart
; chips incorporated on the PCB 

; NB LCD routines
; This HAS to run in CHIP RAM as it turns OFF EXTERNAL memory whilst executing
; so external memory is not accessable.  THis means that any text you need to 
; write first needs to be copied to CHIP RAM and the address and length written 
; to a  some pointer address.  Aaargghhhh!!!!!
; First copy the message to Message area in EEPROM then do LCD routine
; NBB:  This means starting the Handyboard in Special Test Mode but this means 
; that the interrupt table doesnt exist in EEPROM but in the middle of memory!
; I suspect I will interface the LCD differently on a new board so most of this
; code will be redundant.  Oh well, new code new problems.

; $Id: sensor2.asm,v 1.1.1.1 2003/09/15 12:17:40 mikee Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Include 68HC11 register definition file
		.include	"6811regs.asm"	; The registers
		.include	"irqs.asm"	; The interrupt vectors in RAM
		.include	"SpecialTestVectors.asm" 


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Take care here as this may clobber the talker, but a proportion of the talker
; code here only runs once to init some regs etc.

		.section	ram,"ax"


Main:
; Set up a jump table in local RAM so we know where to point the jump vectors
		jmp	Start		; Jump to the code start
RAMBASE		=	.		; The current pointer location. Doesn't really point to RAMBASE

Message:	.asciz 	"0123456789ABCDEF0123456789ABCDEF" 	; 32 char Temporary test
								; area in EEPROM
;------------------------------------------------------------------------------------
; This section MUST be in internal RAM along with the text to be writ

EnableLCD:  
	ldx 	#REGS
	jsr 	Beep  
        sei             		; disable interrupts
	
        ; Set the board to single chip mode  
        bclr    HPRIO,x, #0b00100000   	; Clears the MDA bit, Mode Select A
        
        bset    DDRA,x,  #0b11110000    ; Set port A bit 4  to output
        bclr    PORTA,x, #0b00010000    ; turn off LCD E line
        
        ldaa    #0xFF
        staa    DDRC,x          	; make port C output
        
        ldaa    #0x00           	; In command mode
        ldab    #0x30           	; Power on LCD
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     dly240u
        stab    PORTC,x         	; low byte is data
        jsr     dly240u
        bclr    PORTA,x, #0b00010000
        
        jsr    	dly10
        
        
        ldaa    #0x00           	; In command mode
        ldab    #0x34                   ; Sets LCD to two lines, 8bit, or
	ldab    #0x30                   ; Sets LCD to two lines, 8bit
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     dly240u
        stab    PORTC,x         	; low byte is data
        jsr     dly240u
        bclr    PORTA,x, #0b00010000
        jsr     dly10
        
        
        ldaa    #0x00
        ldab    #0x0e                   ;Switch on display
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr    	dly240u
        stab    PORTC,x         	; low byte is data
        jsr     dly240u
        bclr    PORTA,x, #0b00010000
        jsr     dly10
        
        
        ldaa    #0x00      
        ldab    #0x06                   ;Set LCD to inc & shift on write
        staa    PORTB,x         	; high byte is control
        bset    PORTA,x, #0b00010000
        jsr     dly240u
        stab    PORTC,x         	; low byte is data
        jsr     dly240u
        bclr    PORTA,x, #0b00010000
        jsr     dly10
        
        
        ldaa    #0x00
       	ldab    #0x01                   ; Goto start"
       	staa    PORTB,x         	; high byte is control
       	bset    PORTA,x, #0b00010000
       	jsr  	dly240u
       	stab    PORTC,x         	; low byte is data
       	jsr 	dly240u
       	bclr   	PORTA,x, #0b00010000
       	ldab   	#0x00          		;Prep to set data = 0
       	jsr 	dly240u
       	stab   	PORTC,x            	;Set data = 0
       	jsr    	dly10
		rts			; Return 

;--------------------------------------------------------------------------------
;Write to the LCD Char to be writ is in B
LCDwrite:
	  
        pshx                    	;save it
	psha
	ldx     #REGS         	;The regs base
	ldaa    #0x02
	staa    PORTB,x         	; high byte is control
	jsr     dly240u
	bset    PORTA,x, #0b00010000    ; Set E = HI
	jsr     dly240u
	stab    PORTC,x         	; Send data to LCD
	ldab    #0x00           	; Prep to set data = 0
	jsr     dly240u       		; Wait for it to stabilise
	bclr    PORTA,x, #0b00010000    ; Set E = LO so latching data into 
					; lcd
	jsr     dly240u
	stab    PORTC,x         	;Set data = 0
	bclr	PORTC,X, #0b00000001
	pula
        pulx
        rts      

;----------------------------------------------------------------------------
;Write some data to the screen.  The data are always at the same place, copied there previously

WriteLCD:     
	pshx
	jsr EnableLCD		; Switch to single chip mode and setup LCD
	ldx #Message  		; Point X to the string 
NextChar:       
        ldab    0,x		; The char to be written	
        beq     1f	  	; End of string = '\0'
	pshx
        jsr     LCDwrite    	; Write it to the LCD
	pulx
        inx
        jsr 	dly240u
        bra 	NextChar	; Loop 'till we see a \0

1:
	pulx
	jsr 	ResetLCD	; Return to expanded mode
        rts
                
;----------------------------------------------------------------------------
 ResetLCD:
 	  
 	bset    PORTA,x, #0b00000000
        bclr    DDRA,x,  #0b00001000    ; Set port A bit 4  to input
        
        ; put back into expanded chip mode
        bset    HPRIO,x #0b00100000 	; Set the MDA bit, Mode Select A
        cli 				; enable interrupts
        rts     			; return 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Some delay routines.  Times are in ms
; This has to be in RAM as is used by the LCD write routines 
dly10:
	pshx
	ldx	#0x0A
	jsr	dly
	pulx
	rts
dly20:
	pshx
	ldx	#0x14
	jsr	dly
	pulx
	rts
dly240:
	pshx
	ldx #240     ;240ms
  	jsr dly
	pulx
	rts
dly:
	ldy	#0x011C		;timing assumes 8MHz xtal
1:	dey
	bne	1b
	dex
	bne	dly
	rts
	
dly1m:
	pshy
        ldy #286    ;1ms
        bra wm10
dly720u:
	pshy
        ldy #100    ;720us
        bra wm10
dly240u:
	pshy
        ldy #30     ;240us
        bra wm10
dly90u:
	pshy
        ldy #10     ;90us

wm10:
        dey             ;* 4
        bne wm10        ;* 3
	puly
        rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; program area = external RAM, variable storage 32K

                .section ramex,"ax"     
	bra 	Start			; reset vector points here, = 0xfe00, or will in EEPROM
; Bunch of declarations, storage etc.
					; jmp = 3 bytes.  bra=2 bytes in same page only
inChar:		.byte	0		; Whatever the SCI received
spTemp:		.byte	0		; temp SP value
ASCII:		.ascii	"01"		; A 2 letter ascii number
DBUFR:          .ascii  "01234" 	; 5 byte decimal ASCII number.  NOT an output string holder
DecWhole:	.ascii	"01234"		; Five bytes for whole decimal parts
Point:		.ascii	"."		; The decimal point
DecFract:	.ascii	"01"		; Two byte fraction part
CrLf:		.asciz	"\n\r"		; CrLr and zero string terminator
Thermo:		.byte	0,0,0,0		; four bytes for the temperature in decimal hex
LASTDATA:	.byte	0,0		; Address of the last data stored,  Must be less than MAXDATA
MAXDATA		=	0xfdff		; Highest writable address for data.  DATA starts at program end
MOTORS		=	0x7000		; The motor controller address
TIME		=	0x01c9
COUNTER:	.byte	0,0		; Counter register for timing data, 0x0727 = 1 min

; Preset message list.  To be copied to internal RAM for display must be < 32 chars
Message0:	.asciz	""
Message1:	.asciz	"HIGH EXHAUST TEMP"
Message2:	.asciz	"LOW OIL PRESSURE"
Message3:	.asciz	"LOW BATTERIES"
Message4:	.asciz	"HIGH WATER TEMPERATURE"
Message5:	.asciz	"GAS IN BILGES"
Message6:	.asciz	"OTHER"
Message7:	.asciz	"FAULT"
Message8:	.asciz	"OK"
Message9:	.asciz	"ABANDON SHIP NOW"
Message10:	.asciz	"Fuel Flow"
DISPMODE:	.byte	0	; A number determining what display mode we are in.  256 modes in bits
				; 0 = Default start up mode
				; 1 = Display Engine water temperature
				; 2 = Display Engine exhaust temperature
				; 3 = Display Engine water & exhaust temperature
				; 4 = Display Engine oil pressure
				; 5 =
				; 6 =
					
;Alarm levels need to established but not neccessariy fixed.  Should be user configurable.
; These are placeholder/example data at present
ExHi		= 95	; Degrees C 	
OilLo		= 10	; lbs/sq in
WaterLo		= 5	; Some unit
BattLo		= 8	; Volts
GasHi		= 110	; ppm ????
 


		
; It's important to note here that some of the setup stuff here are to write once registers
; that have to be programmed in the first 64 cycles.  Since we have jumped here from EEPROM 
; we have already used 3 cycles.  Keep a count of the cycles in the init code where the limit applies.
; Don't put non-critical code here even though this means splitting instruction pairs.
Start:
	lds	#0x03ff		; 3, Set new stack pointer to internal RAM top
	bset	OPTION,x,#0x03	; 7, Set the COP timpout to 1.049s, i.e. longest possible
	ldx	#REGS		; 3, set up asyc I/O on SCI
	bset	PACTL,x,0x03	; 7, set to 32.768 ms, 1831 per minute
; End of time critical stuff

	bclr 	CONFIG,x,#0x04	; Set the NOCOP = 0, turn COP system ON
	sei			; Set Interrupt Mask
	bset 	SPCR,x,#0x20	; 
	bset	SCCR2,x,#0x0c	;
	ldd	#0x300c		; 
	staa	BAUD,x		;
	stab	SCCR2,x
	clr 	DATA		; Clear data just in case we get a dump call before logging
	ldx	#DATA		; The firt data is the last data on reset
	stx	LASTDATA	
	ldaa	#0
	staa	SCCR1,x
	staa	inChar		; Zero our storage.  Since A=0 we can do...
	;jsr	Beep
Regreet:
	jsr	SendGreet
	;jsr	StartTemp	; By sefault start logging on bootup
	bra 	Monitor		; Go to the monitor loop
STOP:
	stop			;STOP
	jmp Start		;Do it again. (and prevents continue into code)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Monitor loop for input/output monitoring after the initial set up
; Monitors RS232, A/D, digital inputs and switches on HB
Monitor:
	jsr	INSCI
	jsr	DoInstruction	; Check the pressed key and execute whatever
	jsr	DogWalk
	bra	Monitor
;----------------------------------------------------------------------------
; Walk th dog.  Service the COP 
DogWalk:
	ldaa	#0x55		; reset the COP watchdog, if enabled
	staa	COPRST+REGS
	ldaa 	#0xAA
	staa	COPRST+REGS
	rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Send a logon greeting message
Greeting:		.asciz	"\n\rShip Monitor $Revision: 1.1.1.1 $\n\n\r"
SendGreet:
	ldx	#Greeting	; Point to greeting string
	bra	SendChar	; For completeness, could just run though.
;----------------------------------------------------------------------------
; Send a string out the RS232 port X contains the address
SendChar:
	ldab	0,x
	beq	SendGreetEND	; End of string if 0, so return
	stab	MOTORS		; Just for testing, flashes LEDS
	inx
	jsr	dly20
	jsr	OUTSCI		;
	bra	SendChar
SendGreetEND:	
	rts			;return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Put a character to SCI from B register, clobbers A
OUTSCI:
	pshx
	ldx	#REGS
	psha

LOOPB:
	ldaa	REGS+SCSR	; Wait for TDRE
	anda	#TDRE		; output ready?
	beq	LOOPB		; not yet
	stab	REGS+SCDR	; send it,
	pula
	pulx
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get character from SCI into B register
INSCI:
	ldx	#REGS
	ldab    REGS+SCSR       ; Wait for RDRF
        andb    #RDRF           ; character available?
        beq     NO_CHAR		; not yet
        ldab    REGS+SCDR       ; get it
	jsr 	OUTSCI		; Echo it first
	stab	inChar
	rts
NO_CHAR:	
	clr	inChar		; Clear the input char buffer
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Timer interrupt routine.  Decrements the COUNTER.  Runs DoTemp when COUNTER = 0
; then resets the counter to TIME
T_SIRQ:		
	ldaa	#0x40
	staa	TFLG2+REGS
	ldx	COUNTER
	dex
	beq	1f
	stx	COUNTER
	rti
1:	jsr	DoTemp		; if the time is up take the temperature
	ldd	#TIME		; Reset the counter
	std	COUNTER
		
	rti

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get a value from the A/D input
; Input method/port is defined by the contents of Acc B
; The method of A/D is defined by the contents of ADCTL in the following way
; | CCF | 0 | SCAN | MULT | CD | CC | CB | CA |
;
; CCF = Conversion complete flag
; SCAN, 1 = Convert group continuously, 0 = one convert then stop
; MULT, 1 = Convert four channels in group, 0 = convert single channel
; The lower nibble controls the Channel selection if MULT bit = 1
;  __________________________________
; |CD:CC:CB:CA | Channel | Result in |
; |   0000     |   AN0   |   ADR1    |
; |   0001     |   AN1   |   ADR2    |
; |   0010     |   AN2   |   ADR3    |
; |   0011     |   AN3   |   ADR4    |
; |   0100     |   AN4   |   ADR1    |
; |   0101     |   AN5   |   ADR2    |
; |   0110     |   AN6   |   ADR3    |
; |   0111     |   AN7   |   ADR4    |
; |__________________________________|
;
; Other combinations are for system testing
; Bits CD:CC select a conversion group if MULT = 1,
; otherwise if mult = 0 a single channel is converted four times and the result
; is placed in ADR1-4OPTION,x,#0b10000000	;Switch the A/D on
; Bit 0 of result is 0.39% of FSD, or 0.0195V where Vrh = 5V

GetAD:
	ldx	#REGS
	bset	OPTION,x,#0b10000000	; Switch the A/D on
	stab	ADCTL,x			; Select channels and start conversion
	brclr	ADCTL,x,#0x80,.		; Is the CCF bit set?
	ldd	ADR1,x			; Store the data
	ldd	ADR3,x			; 2 bytes a go uses less code
	bclr	OPTION,x,#0b10000000	; Switch the A/D off
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Do the instruction
DoInstruction:
	ldaa	inChar		; load inst, value to accA accB
	cmpa	#0x74		; Is it an t ?
	beq	StartTemp
	cmpa	#0x64		; Is is a d ?
	beq	DumpData
	cmpa	#0x72		; Is it an r ? = RESET
	beq	Start
	cmpa	#0x78		; Is is an x ?
	beq	StopTemp
	cmpa	#0x73		; Is is an s ?
	beq	SetTime
	cmpa	#0x61		; Is it an a ?
	beq	DispAS		; Display ABANODN SHIP on LCD
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Set the temperature sample period in seconds
; Need to do error checking, for numbers, and convert to a 16bit number.
SetTime:

1:	jsr	INSCI		; Check what the user is doing
	cmpb	#13		; ENTER KEY?
	beq	2f
	bra	1b		; loop till done
2:				; User hit enter, so set the time
	rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Start the temperature aquisistion at timed intervals
StartTemp:
	ldd	#TIME		; Set the time between readings
	std	COUNTER
	ldaa	#0x40
	staa	TMSK2+REGS	; Enable the timer
	staa	TFLG2+REGS	; Clear the flag
	cli			; Clear Interrupt Mask
	bra	DoTemp		; DoTemp

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Do the temparature collection and store the raw A/D value in the address pointed to
; by  LASTDATA.  LASTDATA+1 = 0x00 to indicate end of file
DoTemp:
	ldab	#0x14		; Set the A/D mode to get channels 4-7 0x10 for 0-3
	jsr	GetAD
	ldab	ADR1+REGS	; Get the data in analog 1
	ldx	LASTDATA	
	stab	0,x		; Store the raw data
	clr	1,x		; Set next byte to 0x00
	pshx
	jsr	SendData	; Send a copy to the serial port, X points to the data
	pulx
	inx			; increment the pointer
	stx	LASTDATA	; Store it	
	cmpx	#MAXDATA	; Are we at the end of memory?
	beq	StopTemp	; Stop collecting if we are	
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Stop temperature collection
StopTemp:
	clr	TMSK2+REGS	; disable the timer
	sei			; Set Interrupt Mask
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Dump the entire data set out of the RS232 port
DumpData:
	sei			; Stop taking temperatures while dumping data
	ldx	#DATA
2:	ldab	0,x
	;cmpb	#0x00		; If it's 0 then we are at the end of the data set
	beq	1f		; so exit this routine
	pshx			; Save X
	jsr	SendData	; If it's not 0x00 then send it	
	jsr	INSCI		; Check what the user is doing
	cmpb	#3		; ^C?
	beq	1f
	pulx	
	inx			; Next address
	jsr	DogWalk		; Walk the dog	
	bra 	2b		; Do it again
1:	clr	inChar		; Stop collecting if we are
	cli			; Resume interrupt driven stuff
	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Return the temperature via RS232 as ascii
;A/D	C; Sensor is a thermister connected to AD4.  ie 5th from right.  Supply through 47K
; resnet so circuit looks like:

;      |--< 5V supply and A/D reference
;      |
;      /
;      \ 47K resistor
;      /
;      \
;      |
;      |-- > AD input
;      \
;      /
;      \ Thermister, 30K at 25C
;      /
;      |
;     ### GND

; A small conversion table below:
;81	31.5
;82	31
;104	24.9
;105	24.4
;105	24
;110	22.5
;114	19.9
;120	18
;116	19.3


SendData:
	ldab	0,x		; X points to the data so get it
	ldaa 	#0		;
	ldx	#0x3
	idiv			; D/3 -> X
	xgdx			; X -> D.  X contains remainder
	addd	#0xc6		; #0xc6 = -58
	negb			; Do the twos compliment
	ldaa	#0x0		; Clear the hi byte to get rid of the negation bit as not needed
	std	Thermo		; Store the whole degrees part
	xgdx			; Now we do the fractional part.  Remainder to D
	cmpd	#0x2
	bhs	1f		; If greater than 2 then make it 0.5 else
	ldd	#0x3530		; Char 00
	std	DecFract+0
	bra	2f
1:	ldd	#0x3030		; Char 50
	std	DecFract+0

2:	ldx	#Thermo		; Pass it to the hextoASCII routine
	jsr	HTOD16		; Convert the data to ASCII Answer in DBUFR
	ldx	#DBUFR		; Get the answer
	ldd	0,x
	std	DecWhole+0	; Store it in output buffer, whole parts
	ldd	2,x
	std	DecWhole+2
	ldaa	4,x
	staa	DecWhole+4
	ldx	#DecWhole	; and send it out X = address of String
	jsr	SendChar	; the serial port
	rts			; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; HTOD - Subroutine to convert a 16-bit hex number to a
; 5 digit decimal number.
; With addition to convert an 8 bit number
; Uses 5 byte variable "DBUFR" for decimal ASCII result
; On entry X points to hex value to be converted & displayed
; All registers are unchanged upon return
; From Motorolla @ http://www.motorolla.com

HTOD16:		PSHX			;Save registers
		PSHB
		PSHA
		LDD	0,X		;D=hex value to be converted
		LDX	#10000
		IDIV			;D/10,000 -> X; r -> D
		XGDX			;Save r in X; 10,000s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR		;Store in decimal buffer
		XGDX			;r back to D
		LDX	#1000
		IDIV			;D/1,000 -> X; r -> D
		XGDX			;Save r in X; 1,000s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+1		;Store in decimal buffer
		XGDX			;r back to D
1:		LDX	#100		;
		IDIV			;D/100 -> X; r -> D
		XGDX			;Save r in X; 100s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+2		;Store in decimal buffer
		XGDX			;r back to D
		LDX	#10
		IDIV			;D/10 -> X; r in D (B is units digit)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+4		;Store to units digit
		XGDX			;10s digit to D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+3		;Store in decimal buffer
		PULA			;Restore registers
		PULB
		PULX
		RTS			;Return
;------------------------------------------------------------------------------
; Copy some text to the internal RAM ready for display
; X contains the address of the string to be copied
PreCopy:
		pshy
		ldy	#Message	; load Y with the destination address	
2:		ldaa	0,x		; load A with contents of address X					; Is it a zero?
		staa	0,y
		beq	1f		; If it is we are done
		inx			; increment the addresses of src & dest
		iny
		bra	2b		; Loop again
1:		puly
		rts
;------------------------------------------------------------------------------
; Display some text on the LCD.  A test routine to establish methodology.
DispAS:
	pshx
	ldx	#Message9	; The preset message
	jsr	PreCopy		; Copy to internal RAM
	ldx	#Message
	jsr	WriteLCD	; Write the message to LCD
	ldx	#Message
	jsr	SendChar	; As a test
	pulx
	rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;If an illegal opcode is processed(?) then warn the user somehow like a beep
; or something.  This jump needs to be set in EEPROM or in RAM interrupt vector table
IllOpCode:
	jmp	Start			; Jump to program start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Beeps the speaker
Beep:           
	pshx                            	;Save the X & Y  regs
        pshy
       
        ldx     #REGS                   ;Set the register base address
        ldaa    #0x01                   ;Set the OM5 to toggle on compare
        staa    TCTL1+REGS
        bclr    PACTL,x, #0b00000100     ;Clear I4/05 bit
        ldy     #0x200                  ;Set Y to give beep length
                                                                                ;rts
1:	ldd     TCNT,x                  ;Get timer value
        addd    #0x200                  ;Set beep "frequency" in ticks
        std     TI4O5,x                 ;Store in TOC5
        ldaa    #0b00001000             ;Set to a bit mask 00100000
        staa    TFLG1,x                 ;Clear the TOC5 flag

flgchk$:        
	brclr   TFLG1,x, #0x08, flgchk$ ;Check if flag is zero and loop till = 1
        dey                             ;Dec Y
        bne     1b                   	;Do 'till Y=0
        ldaa    #0x10                   ;Reset the OM5 to do set to 0 on
        staa    TCTL1,x         	;compare
       
        puly                		;Restore the X&y reg
        pulx                		;
        rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DATA		=.			; Start of the data storage area

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This section runs first, on reset and is located in EEPROM
; Set up to jump to start on normal boot
		;.section	eeprom,"ax"
		;jmp	Main

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;$Log: sensor2.asm,v $
;Revision 1.1.1.1  2003/09/15 12:17:40  mikee
;
;

; File t-log.asm
; Derived from robot.asm.
; A temperature logging program that sends data to a host via rs232 and logs
; to memory
;
; $Id: t-log.asm,v 1.1.1.1 2003/09/15 12:17:40 mikee Exp $
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                .title	t-log.asm


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Include 68HC11 register definition file
		.include	"6811regs.asm"	; The registers
		.include	"irqs.asm"	; The interrupt vectors in RAM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Take care here as this may clobber the talker, but a proportion of  the talker
; code here only runs once to init some regs etc.

		.section	ram,"ax"


Main:
; Set up a jump table in local RAM so we know where to point the jump vectors
		jmp	Start		; Jump to the code start
RAMBASE		=	.		; The current pointer location. Doesn't really point to RAMBASE
inChar:		.byte	0x01		; Whatever the SCI received
spTemp:		.byte	0x02		; temp SP value
ASCII:		.ascii	"01"		; A 2 letter ascii number
DBUFR:          .ascii  "01234" 	; 5 byte decimal ASCII number.  NOT an output string holder
DecWhole:	.ascii	"01234"		; Five bytes for whole decimal parts
Point:		.ascii	"."		; The decimal point
DecFract:	.ascii	"01"		; Two byte fraction part
CrLf:		.asciz	"\n\r"		; CrLr and zero string terminator
Thermo:		.byte	0,0,0,0		; four bytes for the temperature in decimal hex
LASTDATA:	.byte	0,0		; Address of the last data stored,  Must be less than MAXDATA
MAXDATA		=	0xfdff		; Highest writable address for data.  DATA starts at program end
MOTORS		=	0x7000		; The motor controller address
TIME		=	0x01c9
COUNTER:	.byte	0,0		; Counter register for timing data, 0x0727 = 1 min

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                .section ramex,"ax"     ; program area external RAM, variable storage 32K
		
; It's important to note here that some of the setup stuff here are to write once registers
; that have to be programmed in the first 64 cycles.  Since we have jumped here from EEPROM 
; we have already used 3 cycles.  Keep a count of the cycles in the init code where the limit applies.
; Don't put non-critical code here even though this means splitting instruction pairs.
Start:
		lds	#0x03ff		; 3, Set new stack pointer to internal RAM top
		;bset	OPTION,x,#0x03	; 7, Set the COP timpout to 1.049s, i.e. longest possible
		ldx	#REGS		; 3, set up asyc I/O on SCI
		bset	PACTL,x,0x03	; 7, set to 32.768 ms, 1831 per minute
		; End of time critical stuff
		;bclr 	CONFIG,x,#0x04	; Set the NOCOP = 0, turn COP system ON
		
		bset 	SPCR,x,#0x20	; 
		bset	SCCR2,x,#0x0c	;
		ldd	#0x300c		; 
		staa	BAUD,x		;
		stab	SCCR2,x
		clr 	DATA		; Clear data just in case we ger a dump call before logging
		ldx	#DATA		; The firt data is the last data on reset
		stx	LASTDATA	
		ldaa	#0
		staa	SCCR1,x
		staa	inChar		; Zero our storage.  Since A=0 we can do...
Regreet:
		jsr	SendGreet
		;jsr	StartTemp	; By sefault start logging on bootup
		bra 	Monitor		; Go to the monitor loop
STOP:
		;lds	spTemp		;restore the SP
		stop			;STOP
		jmp Start		;Do it again. (and prevents continue into code)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Monitor loop for input/output monitoring after the initial set up
; Monitors RS232, A/D, digital inputs and switches on HB
Monitor:
		jsr	INSCI
		jsr	DoInstruction	; Check the pressed key and execute whatever
		ldaa	#0x55		; reset the COP watchdog, if enabled
		staa	COPRST+REGS
		ldaa 	#0xAA
		staa	COPRST+REGS
		bra	Monitor

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Send a logon greeting message
Greeting:		.asciz	"T-LOGGER $Revision: 1.1.1.1 $\n\n\r"
SendGreet:
		ldx	#Greeting	; Point to greeting string
SendChar:
		ldab	0,x
		beq	SendGreetEND	; End of string if 0, so return
		stab	MOTORS		; Just for testing, flashes LEDS
		inx
		jsr	dly20
		;jsr	dly20
		jsr	OUTSCI		;
		bra	SendChar
SendGreetEND:	rts			;return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Put a character to SCI from B register, clobbers A
OUTSCI:
		pshx
		ldx	#REGS
		psha

LOOPB:
		ldaa	REGS+SCSR	; Wait for TDRE
		anda	#TDRE		; output ready?
		beq	LOOPB		; not yet
		stab	REGS+SCDR	; send it,
		pula
		pulx
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get character from SCI into B register
INSCI:
		ldx	#REGS
		ldab    REGS+SCSR       ; Wait for RDRF
                andb    #RDRF           ; character available?
                beq     NO_CHAR		; not yet
                ldab    REGS+SCDR       ; get it
		jsr 	OUTSCI		; Echo it first
		stab	inChar
		rts
NO_CHAR:	clr	inChar		; Clear the input char buffer
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Timer interrupt routine.  Decrements the COUNTER.  Runs DoTemp when COUNTER = 0
; then resets the counter to TIME
T_SIRQ:		ldaa	#0x40
		staa	TFLG2+REGS
		ldx	COUNTER
		dex
		beq	1f
		stx	COUNTER
		rti
1:		jsr	DoTemp		; if the time is up take the temperature
		ldd	#TIME		; Reset the counter
		std	COUNTER
		
		rti

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get a value from the A/D input
; Input method/port is defined by the contents of Acc B
; The method of A/D is defined by the contents of ADCTL in the following way
; | CCF | 0 | SCAN | MULT | CD | CC | CB | CA |
;
; CCF = Conversion complete flag
; SCAN, 1 = Convert group continuously, 0 = one convert then stop
; MULT, 1 = Convert four channels in group, 0 = convert single channel
; The lower nibble controls the Channel selection if MULT bit = 1
;  __________________________________
; |CD:CC:CB:CA | Channel | Result in |
; |   0000     |   AN0   |   ADR1    |
; |   0001     |   AN1   |   ADR2    |
; |   0010     |   AN2   |   ADR3    |
; |   0011     |   AN3   |   ADR4    |
; |   0100     |   AN4   |   ADR1    |
; |   0101     |   AN5   |   ADR2    |
; |   0110     |   AN6   |   ADR3    |
; |   0111     |   AN7   |   ADR4    |
; |__________________________________|
;
; Other combinations are for system testing
; Bits CD:CC select a conversion group if MULT = 1,
; otherwise if mult = 0 a single channel is converted four times and the result
; is placed in ADR1-4OPTION,x,#0b10000000	;Switch the A/D on
; Bit 0 of result is 0.39% of FSD, or 0.0195V where Vrh = 5V

GetAD:
		ldx	#REGS
		bset	OPTION,x,#0b10000000	; Switch the A/D on
		stab	ADCTL,x			; Select channels and start conversion
		brclr	ADCTL,x,#0x80,.		; Is the CCF bit set?
		ldd	ADR1,x			; Store the data
		ldd	ADR3,x			; 2 bytes a go uses less code
		bclr	OPTION,x,#0b10000000	; Switch the A/D off
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Do the instruction
DoInstruction:
		ldaa	inChar		; load inst, value to accA accB
		cmpa	#0x74		; Is it an t ?
		beq	StartTemp
		cmpa	#0x64		; Is is a d ?
		beq	DumpData
		cmpa	#0x72		; Is it an r ? = RESET
		beq	Start
		cmpa	#0x78		; Is is an x ?
		beq	StopTemp
		cmpa	#0x73
		beq	SetTime
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Set the temperature sample period in seconds
; Need to do error checking, for numbers, and convert to a 16bit number.
SetTime:

1:		jsr	INSCI		; Check what the user is doing
		cmpb	#13		; ENTER KEY?
		beq	2f
		bra	1b		; loop till done
2:					; User hit enter, so set the time
		rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Start the temperature aquisistion at timed intervals
StartTemp:
		ldd	#TIME		; Set the time between readings
		std	COUNTER
		ldaa	#0x40
		staa	TMSK2+REGS	; Enable the timer
		staa	TFLG2+REGS	; Clear the flag
		cli			; Clear Interrupt Mask
		;bra	DoTemp		; just run through to DoTemp

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Do the temparature collection and store the raw A/D value in the address pointed to
; by  LASTDATA.  LASTDATA+1 = 0x00 to indicate end of file
DoTemp:
		ldab	#0x14		; Set the A/D mode to get channels 4-7
		jsr	GetAD
		ldab	ADR1+REGS	; Get the data in analog 1
		ldx	LASTDATA
		stab	0,x		; Store the raw data
		clr	1,x		; Set next byte to 0x00
		inx			; increment the pointer
		stx	LASTDATA	; Store it
		dex
		jsr	SendData	; Send a copy to the serial port, X points to the data
		cmpx	#MAXDATA		; Are we at the end of memory?
		beq	StopTemp		; Stop collecting if we are
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Stop temperature collection
StopTemp:	clr	TMSK2+REGS	; disable the timer
		sei			; Set Interrupt Mask
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Dump the entire data set out of the RS232 port
DumpData:	sei			; Stop taking temperatures while dumpig data
		ldx	#DATA
2:		ldab	0,x
		cmpb	#0x00
		beq	1f
		pshx			; Save X
		jsr	SendData	; If it's not 0x00 then send it
		pulx
		jsr	INSCI		; Check what the user is doing
		cmpb	#3		; ^C?
		beq	1f
		
		inx			; Next address
		bra 	2b		; Do it again
1:		clr	inChar		; Stop collecting if we are
		cli			; Resume interrupt driven stuff
		rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Return the temperature via RS232
; A small conversion table below:
;A/D	C
;81	31.5
;82	31
;104	24.9
;105	24.4
;105	24
;110	22.5
;114	19.9
;120	18
;116	19.3


SendData:
		ldab	0,x		; X points to the data so get it
		ldaa 	#0		;
		ldx	#0x3
		idiv			; D/3 -> X
		xgdx			; X -> D.  X contains remainder
		addd	#0xc6		; #0xc6 = -58
		negb			; Do the twos compliment
		ldaa	#0x0		; Clear the hi byte to get rid of the negation bit as not needed
		std	Thermo		; Store the whole degrees part
		xgdx			; Now we do the fractional part.  Remainder to D
		cmpd	#0x2
		bhs	1f		; If greater than 2 then make it 0.5 else
		ldd	#0x3530		; Char 00
		std	DecFract+0
		bra	2f
1:		ldd	#0x3030		; Char 50
		std	DecFract+0

2:		ldx	#Thermo		; Pass it to the hextoASCII routine
		jsr	HTOD16		; Convert the data to ASCII Answer in DBUFR
		ldx	#DBUFR		; Get the answer
		ldd	0,x
		std	DecWhole+0	; Store it in output buffer, whole parts
		ldd	2,x
		std	DecWhole+2
		ldaa	4,x
		staa	DecWhole+4
		ldx	#DecWhole	; and send it out X = address of String
		jsr	SendChar	; the serial port
		rts			; return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Some delay routines.  Times are in ms
dly10:
		pshx
		ldx	#0x0A
		jsr	dly
		pulx
		rts
dly20:
		pshx
		ldx	#0x14
		jsr	dly
		pulx
		rts

dly:
		ldy	#0x011C		;timing assumes 8MHz xtal
1:		dey
		bne	1b
		dex
		bne	dly
		rts
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; HTOD - Subroutine to convert a 16-bit hex number to a
; 5 digit decimal number.
; With addition to convert an 8 bit number
; Uses 5 byte variable "DBUFR" for decimal ASCII result
; On entry X points to hex value to be converted & displayed
; All registers are unchanged upon return
; From Motorolla @ http://www.motorolla.com

HTOD16:		PSHX			;Save registers
		PSHB
		PSHA
		LDD	0,X		;D=hex value to be converted
		LDX	#10000
		IDIV			;D/10,000 -> X; r -> D
		XGDX			;Save r in X; 10,000s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR		;Store in decimal buffer
		XGDX			;r back to D
		LDX	#1000
		IDIV			;D/1,000 -> X; r -> D
		XGDX			;Save r in X; 1,000s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+1		;Store in decimal buffer
		XGDX			;r back to D
1:		LDX	#100		;
		IDIV			;D/100 -> X; r -> D
		XGDX			;Save r in X; 100s digit in D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+2		;Store in decimal buffer
		XGDX			;r back to D
		LDX	#10
		IDIV			;D/10 -> X; r in D (B is units digit)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+4		;Store to units digit
		XGDX			;10s digit to D (A:B)
		ADDB	#0x30		;Convert to ASCII
		STAB	DBUFR+3		;Store in decimal buffer
		PULA			;Restore registers
		PULB
		PULX
		RTS			;Return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;If an illegal opcode is processed(?) then warn the user somehow like a beep
; or something.  This jump needs to be set in EEPROM or in RAM interrupt vector table
;IllOpCode:
;	jmp	0xFE00			; Jump to program start

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DATA		=.			; Start of the data storage area

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This section runs first, on reset and is located in EEPROM
; Set up to jump to start on normal boot
		;.area   CODE2	(ABS)
		;.org	0xfe00
		;.section	eeprom,"ax"
		;jmp	Main

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;$Log: t-log.asm,v $
;Revision 1.1.1.1  2003/09/15 12:17:40  mikee
;
;
;Revision 1.3  2003/03/29 11:35:08  mikee
;Working with fixed sample time, 15s.
;TODO: Add help and time setting functions
;
;Revision 1.2  2003/03/28 19:47:37  mikee
;Working but time sampling needs doing
;
;Revision 1.1  2003/03/28 16:17:46  mikee
;First check in
;

